<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobSideImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_side_images', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('direction', ['Left', 'Right']);
            $table->string('picturePath');
            $table->enum('status', ['Active', 'Deactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_side_images');
    }
}
