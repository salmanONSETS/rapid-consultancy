<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.images');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $image = \App\Image::where('status', 'Active')->where('page', 'Welcome')->where('number', $request->get('number'))->first();
      if(empty($image)){
        $image = new \App\Image;
      }
        if($request->get('page') == 'Home'){
          if($request->get('number') == ''){
              $image->number = '4';
          }
          else{
            $image->number = $request->get('number');
          }
        }
        $image->status = 'Active';
        $image->page = $request->get('page');

        if($request->hasFile('image')){
          $destinationImages='imagesData/images';
          $allowedImagefileExtension=['jpg','png','jpeg'];
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            if($image->page == 'Home'){
              $image1 = \App\Image::where('status', 'Active')->where('page', 'Welcome')->where('number', $image->number)->first();
              if(!empty($image1)){
                $path = 'imagesData/images/'.$image1->picturePath;
                File::delete($path);
              }
              $filename = $image->number.'.'.$extension;
            }
            $file->move($destinationImages,$filename);
            $image->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('images')->with('message', $message);
          }
        }

        $image->save();
        if($image->page != 'Home'){
          $images = \App\Image::where('status', 'Active')->where('page', $image->page)->where('id', '!=', $image->id)->get();
          foreach ($images as $key => $value) {
            $value->status = 'Deactive';
            $value->save();
          }
        }
        return redirect('images');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = \App\Image::find($id);
        return view('dashboard.images', ['image' => $image]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = \App\Image::find($id);
        $image->page = $request->get('page');

        if($request->hasFile('image')){
          $destinationImages='imagesData/images';
          $allowedImagefileExtension=['jpg','png','jpeg'];
          if(!empty($image->picturePath)){
            $path = 'imagesData/images/'.$image->picturePath;
            $file=Input::file("image");
            $extension = $file->getClientOriginalExtension();
            $checkImage=in_array($extension,$allowedImagefileExtension);
            $filename = time() . '-' .$file->getClientOriginalName();
            if($checkImage){
              $file->move($destinationImages,$filename);
              $image->picturePath = $filename;
            }
            File::delete($path);
          }
          else{
            $file=Input::file("image");
            $extension = $file->getClientOriginalExtension();
            $checkImage=in_array($extension,$allowedImagefileExtension);
            $filename = time() . '-' .$file->getClientOriginalName();
            if($checkImage){
              if($image->page == 'welcome'){
                $image1 = \App\Image::where('status', 'Active')->where('page', 'Welcome')->where('number', $image->number)->first();
                if(!empty($image1)){
                  $path = 'imagesData/images/'.$image1->picturePath;
                  File::delete($path);
                }
                $filename = $image->number;
              }
              $file->move($destinationImages,$filename);
              $image->picturePath = $filename;
            }
            else{
              $message = 'Only .png, .jpg, .jpeg accepted';
              return redirect('images')->with('message', $message);
            }
          }
        }
        $image->save();
        if($image->page != 'Home' && $image->status == 'Active'){
          $images = \App\Image::where('status', 'Active')->where('page', $image->page)->where('id', '!=', $image->id)->get();
          foreach ($images as $key => $value) {
            $value->status = 'Deactive';
            $value->save();
          }
        }
        return redirect('images');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = \App\Image::find($id);

        if(!empty($image->picturePath)){
          $path = 'imagesData/images/'.$image->picturePath;
          File::delete($path);
        }

        $image->delete();
        return redirect('images');
    }

    public function changestatus($id){
      $image = \App\Image::find($id);
      if($image->status == 'Active'){
        $image->status = 'Deactive';
      }
      else{
        $image->status = 'Active';
      }
      $image->save();
      if($image->status == 'Active'){
        $images = \App\Image::where('status', 'Active')->where('page', $image->page)->where('id', '!=', $image->id)->get();
        foreach ($images as $key => $value) {
          $value->status = 'Deactive';
          $value->save();
        }
      }
      return redirect('images');
    }
}
