<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class CareerTipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.careerTips');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $careerTip = new \App\CareerTip;
        $careerTip->title = $request->get('title');
        $careerTip->detail = $request->get('detail');
        $careerTip->status = 'Active';

        if($request->hasFile('image')){
          $destinationImages='careerTipsData/images';
          $allowedImagefileExtension=['jpg','png','jpeg'];
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $careerTip->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('careerTips')->with('message', $message);
          }
        }

        $careerTip->save();
        return redirect('careerTips');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $careerTip = \App\CareerTip::find($id);
        return view('dashboard.careerTips', ['careerTip' => $careerTip]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $careerTip = \App\CareerTip::find($id);
      $careerTip->title = $request->get('title');
      $careerTip->detail = $request->get('detail');

      if($request->hasFile('image')){
        $destinationImages='careerTipsData/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        if(!empty($careerTip->picturePath)){
          $path = 'careerTipsData/images/'.$careerTip->picturePath;
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $careerTip->picturePath = $filename;
          }
          File::delete($path);
        }
        else{
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $careerTip->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('careerTips')->with('message', $message);
          }
        }
      }

      $careerTip->save();
      return redirect('careerTips');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $careerTip = \App\CareerTip::find($id);
      if(!empty($careerTip->picturePath)){
        $path = 'careerTipsData/images/'.$careerTip->picturePath;
        File::delete($path);
      }
      $careerTip->delete();
      return redirect('careerTips');
    }

    public function changestatus($id){
      $careerTip = \App\CareerTip::find($id);
      if($careerTip->status == 'Active'){
        $careerTip->status = 'Deactive';
      }
      else{
        $careerTip->status = 'Active';
      }
      $careerTip->save();
      return redirect('careerTips');
    }
}
