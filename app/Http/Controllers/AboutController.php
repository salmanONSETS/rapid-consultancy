<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.abouts');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $about = new \App\About;
        $about->title = $request->get('title');
        $about->detail = $request->get('detail');
        $about->status = 'Active';
        if($request->hasFile('image')){     
            $destinationImages='aboutpage/images';
            $allowedImagefileExtension=['jpg','png','jpeg'];
            $file= Input::file("image");
            $extension = $file->getClientOriginalExtension();
            $checkImage=in_array($extension,$allowedImagefileExtension);
            $filename = time() . '-' .$file->getClientOriginalName();
            if($checkImage){
                if(!empty($about->picturePath)){
                    $path = 'aboutpage/images/'.$about->picturePath;
                    File::delete($path);
                }
                $file->move($destinationImages,$filename);
                $about->picturePath = $filename;
            }
            else{
                $message = 'Only .png, .jpg, .jpeg accepted';
                return redirect('abouts')->with('message', $message);
            }
        }
        $about->save();
        return redirect('abouts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = \App\About::find($id);
        return view('dashboard.abouts', ['about' => $about]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $about = \App\About::find($id);
        $about->title = $request->get('title');
        $about->detail = $request->get('detail');
        $about->status = 'Active';
        if($request->hasFile('image')){
            $destinationImages='aboutpage/images';
            $allowedImagefileExtension=['jpg','png','jpeg'];
            $file= Input::file("image");
            $extension = $file->getClientOriginalExtension();
            $checkImage=in_array($extension,$allowedImagefileExtension);
            $filename = time() . '-' .$file->getClientOriginalName();
            if($checkImage){
                if(!empty($about->image)){
                    $path = 'aboutpage/images/'.$about->image;
                    File::delete($path);
                }
                $file->move($destinationImages,$filename);
                $about->image = $filename;
            }
            else{
                $message = 'Only .png, .jpg, .jpeg accepted';
                return redirect('abouts')->with('message', $message);
            }
        }
        $about->save();
        return redirect('abouts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about = \App\About::find($id);
        if(!empty($about->picturePath)){
            $path = 'aboutpage/images/'.$about->picturePath;
            File::delete($path);
          }
        $about->delete();
    }

    public function changestatus($id){
        $about = \App\About::find($id);
        if($about->status == 'Active'){
          $about->status = 'Deactive';
        }
        else{
          $about->status = 'Active';
        }
        $about->save();
        return redirect('abouts');
      }
  
      public function deleteLargeImage($id){
        $about = \App\About::find($id);
        if(!empty($about->largeImage)){
          $path = 'aboutpage/images/'.$about->picturePath;
          File::delete($path);
          $about->largeImage = null;
          $about->save();
        }
        return redirect('abouts');
      }
}
