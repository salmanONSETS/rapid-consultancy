<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.messages');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = new \App\Message;
        $message->name = $request->get('name');
        $message->phone = $request->get('phone');
        $message->email = $request->get('email');
        $message->message = $request->get('message');
        $message->save();
        $notification = \App\Notification::where('status', 'Active')->where('useage', 'Contact')->first();
        if(!empty($notification)){
          $message = $notification->message;
        }
        else{
          $message = 'Message Sent';
        }
        return redirect('/contactUs')->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = \App\Message::find($id);
        return view('dashboard.messages', ['message' => $message]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = \App\Message::find($id);
        return view('dashboard.messages', ['message' => $message]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = \App\Message::find($id);
        $message->name = $request->get('name');
        $message->phone = $request->get('phone');
        $message->email = $request->get('email');
        $message->message = $request->get('message');
        $message->save();
        return redirect('messages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = \App\Message::find($id);
        $message->delete();
        return redirect('messages');
    }

    public function changestatus($id){
        $message = \App\Message::find($id);
        if($message->status == 'Active'){
          $message->status = 'Deactive';
        }
        else{
          $message->status = 'Active';
        }
        $message->save();
        return redirect('messages');
    }
}
