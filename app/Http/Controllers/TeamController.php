<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.teams');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teamMember = new \App\Team;
        $teamMember->name = $request->get('name');
        $teamMember->designation = $request->get('designation');
        $teamMember->status = 'Active';

        if($request->hasFile('image')){
          $destinationImages='teamsData/images';
          $allowedImagefileExtension=['jpg','png','jpeg'];
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $teamMember->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('teams')->with('message', $message);
          }
        }

        $teamMember->save();
        return redirect('teams');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teamMember = \App\Team::find($id);
        return view('dashboard.teams', ['teamMember' => $teamMember]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $teamMember = \App\Team::find($id);
      $teamMember->name = $request->get('name');
      $teamMember->designation = $request->get('designation');
      $teamMember->status = 'Active';

      if($request->hasFile('image')){
        $destinationImages='teamsData/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        if(!empty($teamMember->picturePath)){
          $path = 'teamsData/images/'.$teamMember->picturePath;
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $teamMember->picturePath = $filename;
          }
          File::delete($path);
        }
        else{
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $teamMember->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('teams')->with('message', $message);
          }
        }
      }

      $teamMember->save();
      return redirect('teams');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $teamMember = \App\Team::find($id);
      if(!empty($teamMember->picturePath)){
        $path = 'teamsData/images/'.$teamMember->picturePath;
        File::delete($path);
      }
      $teamMember->delete();
      return redirect('teams');
    }

    public function changestatus($id){
      $teamMember = \App\Team::find($id);
      if($teamMember->status == 'Active'){
        $teamMember->status = 'Deactive';
      }
      else{
        $teamMember->status = 'Active';
      }
      $teamMember->save();
      return redirect('teams');
    }
}
