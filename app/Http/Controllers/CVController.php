<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class CVController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.cvs');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cv = \App\CV::find($id);
        return view('dashboard.cvs', ['cv' => $cv]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $cv = \App\CV::find($id);
      if(!empty($cv->cvPath)){
        $path = 'cvsData/images/'.$cv->cvPath;
        File::delete($path);
      }
      $cv->delete();
      return redirect('cvs');
    }

    public function changestatus($id){
      $cv = \App\CV::find($id);
      if($cv->status == 'Active'){
        $cv->status = 'Deactive';
      }
      else{
        $cv->status = 'Active';
      }
      $cv->save();
      return redirect('cvs');
    }

    public function applyForJob(Request $request){
      $cv = new \App\CV;
      $cv->name = $request->get('name');
      $cv->email = $request->get('email');
      $cv->phone = $request->get('phone');
      $cv->comments = $request->get('comments');
      $cv->jobId = $request->get('jobId');
      $cv->status = 'Active';
      $cv->jobStatus = $request->get('jobStatus');

      if($request->hasFile('file')){
        $destinationImages='cvsData/documents';
        $allowedImagefileExtension=['pdf','doc','docx'];
        $file=Input::file("file");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $file->move($destinationImages,$filename);
          $cv->cvPath = $filename;
        }
        else{
          $message = 'Only .pdf, .doc, .docx accepted';
          return redirect('/')->with('message', $message);
        }
      }

      $cv->save();
      $notification = \App\Notification::where('status', 'Active')->where('useage', 'Apply')->first();
      if(!empty($notification)){
        $message = $notification->message;
      }
      else{
        $message = 'Applied';
      }
      return redirect('/')->with(['message' => $message]);
    }
}
