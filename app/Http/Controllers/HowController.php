<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class HowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.hows');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $how = new \App\How;
        $how->status = 'Active';
        $how->serialNumber = $request->get('serialNumber');
        $how->title = $request->get('title');
        $how->detail = $request->get('detail');

        if($request->hasFile('image')){
          $destinationImages='howsData/images';
          $allowedImagefileExtension=['jpg','png','jpeg'];
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $how->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('hows')->with('message', $message);
          }
        }

        $how->save();
        return redirect('hows');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $how = \App\How::find($id);
        return view('dashboard.hows', ['how' => $how]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $how = \App\How::find($id);
      $how->serialNumber = $request->get('serialNumber');
      $how->title = $request->get('title');
      $how->detail = $request->get('detail');

      if($request->hasFile('image')){
        $destinationImages='howsData/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        if(!empty($how->picturePath)){
          $path = 'howsData/images/'.$how->picturePath;
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $how->picturePath = $filename;
          }
          File::delete($path);
        }
        else{
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $how->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('hows')->with('message', $message);
          }
        }
      }

      $how->save();
      return redirect('hows');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $how = \App\How::find($id);
        if(!empty($how->picturePath)){
          $path = 'howsData/images/'.$how->picturePath;
          File::delete($path);
        }
        $how->delete();
        return redirect('hows');
    }

    public function changestatus($id){
      $how = \App\How::find($id);
      if($how->status == 'Active'){
        $how->status = 'Deactive';
      }
      else{
        $how->status = 'Active';
      }
      $how->save();
      return redirect('hows');
    }
}
