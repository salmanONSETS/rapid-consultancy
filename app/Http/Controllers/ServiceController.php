<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.services');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = new \App\Service;
        $service->title = $request->get('title');
        $service->icon = $request->get('icon');
        $service->detail = $request->get('detail');
        $service->status = 'Active';
        $service->save();
        return redirect('services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = \App\Service::find($id);
        return view('dashboard.services', ['service' => $service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $service = \App\Service::find($id);
      $service->title = $request->get('title');
      $service->icon = $request->get('icon');
      $service->detail = $request->get('detail');
      $service->save();
      return redirect('services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = \App\Service::find($id);
        $service->delete();
        return redirect('services');
    }

    public function changestatus($id){
        $service = \App\Service::find($id);
        if($service->status == 'Active'){
          $service->status = 'Deactive';
        }
        else{
          $service->status = 'Active';
        }
        $service->save();
        return redirect('services');
    }
}
