<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.addresses');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $addresses = \App\Address::where('status', 'Active')->where('type', 'Primary')->get();
        if($addresses->isEmpty()){
          $address = new \App\Address;
          $address->address = $request->get('address');
          $address->title = $request->get('title');
          $address->type = $request->get('type');
          $address->status = 'Active';
          $address->save();
        }
        else{
          if($request->get('type') == 'Primary'){
            foreach ($addresses as $key => $address) {
              $address->type = 'Secondary';
              $address->save();
            }
          }
          $address = new \App\Address;
          $address->address = $request->get('address');
          $address->title = $request->get('title');
          $address->type = $request->get('type');
          $address->status = 'Active';
          $address->save();
        }
        return redirect('addresses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $address = \App\Address::find($id);
      return view('dashboard.addresses')->with('address', $address);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $address = \App\Address::find($id);
      $addresses = \App\Address::where('id', '!=', $id)->get();
      if($addresses->isEmpty()){
        $address->address = $request->get('address');
        $address->type = $request->get('type');
        $address->title = $request->get('title');
        $address->status = 'Active';
        $address->save();
      }
      else{
        if($request->get('type') == 'Primary'){
          foreach ($addresses as $key => $value) {
            $value->type = 'Secondary';
            $value->save();
          }
        }
        $address->address = $request->get('address');
        $address->type = $request->get('type');
        $address->title = $request->get('title');
        $address->status = 'Active';
        $address->save();
      }
      return redirect('addresses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $address = \App\Address::find($id);
      $address->delete();
      return redirect('addresses');
    }

    public function changestatus($id){
      $address = \App\Address::find($id);
      if($address->status == 'Active'){
        $address->status = 'Deactive';
      }
      else{
        $address->status = 'Active';
      }
      $address->save();
      return redirect('addresses');
    }
}
