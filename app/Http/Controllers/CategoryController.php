<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.categories');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $category = new \App\Category;
      $category->name = $request->get('name');
      $category->icon = $request->get('icon');
      $category->status = 'Active';

      if($request->hasFile('image')){
        $destinationImages='categoriesData/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $file->move($destinationImages,$filename);
          $category->picturePath = $filename;
        }
        else{
          $message = 'Only .png, .jpg, .jpeg accepted';
          return redirect('categories')->with('message', $message);
        }
      }

      $category->save();
      return redirect('categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = \App\Category::find($id);
        return view('dashboard.categories', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $category = \App\Category::find($id);
      $category->name = $request->get('name');
      $category->icon = $request->get('icon');

      if($request->hasFile('image')){
        $destinationImages='categoriesData/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        if(!empty($category->picturePath)){
          $path = 'categoriesData/images/'.$category->picturePath;
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $category->picturePath = $filename;
          }
          File::delete($path);
        }
        else{
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $category->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('categories')->with('message', $message);
          }
        }
      }

      $category->save();
      return redirect('categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $category = \App\Category::find($id);
      if(!empty($category->picturePath)){
        $path = 'categoriesData/images/'.$category->picturePath;
        File::delete($path);
      }
      $category->delete();
      return redirect('categories');
    }

    public function changestatus($id){
      $category = \App\Category::find($id);
      if($category->status == 'Active'){
        $category->status = 'Deactive';
      }
      else{
        $category->status = 'Active';
      }
      $category->save();
      return redirect('categories');
    }
}
