<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.emails');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = new \App\Email;
        if($request->get('title') != ''){
            $email->title = $request->get('title');
        }
        $email->email = $request->get('email');
        $email->type = $request->get('type');
        $email->status = 'Active';
        $email->save();
        return redirect('emails');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $email = \App\Email::find($id);
        return view('dashboard.emails')->with('email', $email);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $email = \App\Email::find($id);
        $emails = \App\Email::where('id', '!=', $id)->get();
        if($emails->isEmpty()){
            $email->email = $request->get('email');
            $email->title = $request->get('title');
            $email->type = $request->get('type');
            $email->status = 'Active';
            $email->save();
        }
        else{
            if($request->get('type') == 'Primary'){
                foreach ($emails as $key => $value) {
                    $value->type = 'Secondary';
                    $value->save();
                }
            }
            $email->email = $request->get('email');
            $email->type = $request->get('type');
            $email->title = $request->get('title');
            $email->status = 'Active';
            $email->save();
        }
        return redirect('emails');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $email = \App\Email::find($id);
        $email->delete();
        return redirect('emails');
    }

    public function changestatus($id){
        $email = \App\Email::find($id);
        if($email->status == 'Active'){
          $email->status = 'Deactive';
        }
        else{
          $email->status = 'Active';
        }
        $email->save();
        return redirect('emails');
    }
}
