<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class JobSideImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.jobSideImages');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $jobSideImage = new \App\JobSideImage;
      $jobSideImage->status = 'Active';
      $jobSideImage->direction = $request->get('direction');
      if($request->hasFile('image')){
        $destinationImages='jobSideImagesData/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $file->move($destinationImages,$filename);
          $jobSideImage->picturePath = $filename;
        }
        else{
          $message = 'Only .png, .jpg, .jpeg accepted';
          return redirect('jobSideImages')->with('message', $message);
        }
      }
      $actives = \App\JobSideImage::where('status', 'Active')->where('direction', $jobSideImage->direction)->get();
      foreach ($actives as $key => $value) {
        $value->status = 'Active';
        $value->save();
      }
      $jobSideImage->save();
      return redirect('jobSideImages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $jobSideImage = \App\JobSideImage::find($id);
      if(!empty($jobSideImage->picturePath)){
        $path = 'jobSideImagesData/images/'.$jobSideImage->picturePath;
        File::delete($path);
      }
      $jobSideImage->delete();
      return redirect('jobSideImages');
    }

    public function changestatus($id){
      $jobSideImage = \App\JobSideImage::find($id);
      if($jobSideImage->status == 'Active'){
        $jobSideImage->status = 'Deactive';
      }
      else{
        $jobSideImage->status = 'Active';
      }
      $jobSideImage->save();
      return redirect('jobSideImages');
    }
}
