<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.jobs');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $job = new \App\Job;
        $job->title = $request->get('title');
        $job->companyName = $request->get('companyName');
        $job->country = $request->get('country');
        $job->salary = $request->get('salary');
        $job->categoryId = $request->get('categoryId');
        $job->status = 'Active';
        if($request->get('featured') == 'yes'){
          $job->featured = 1;
        }
        else {
          $job->featured = 0;
        }
        $job->save();
        return redirect('jobs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = \App\Job::find($id);
        return view('dashboard.jobs', ['job' => $job]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $job = \App\Job::find($id);
        $job->title = $request->get('title');
        $job->companyName = $request->get('companyName');
        $job->country = $request->get('country');
        $job->salary = $request->get('salary');
        $job->categoryId = $request->get('categoryId');
        if($request->get('featured') == 'yes'){
          $job->featured = 1;
        }
        else {
          $job->featured = 0;
        }
        $job->save();
        return redirect('jobs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = \App\Job::find($id);
        $job->delete();
        return redirect('jobs');
    }

    public function changestatus($id){
      $job = \App\Job::find($id);
      if($job->status == 'Active'){
        $job->status = 'Deactive';
      }
      else{
        $job->status = 'Active';
      }
      $job->save();
      return redirect('jobs');
    }

    public function searchJobs(Request $request){
      $title = $request->get('title');
      $categoryId = $request->get('categoryId');
      $country = $request->get('country');
      $searchedJobs = new Collection();
      if(!empty($title) && !empty($categoryId) && !empty($country)){
        $searchedJobs = \App\Job::where('status', 'Active')->where('categoryId', $categoryId)
        ->where('country', $country)->where('title', 'like', '%'.$title.'%')->get();
      }
      else if(empty($title) && !empty($categoryId) && !empty($country)){
        $searchedJobs = \App\Job::where('status', 'Active')->where('categoryId', $categoryId)
        ->where('country', $country)->get();
      }
      else if(empty($title) && empty($categoryId) && empty($country)){
        return view('front.jobs');
      }
      else if(!empty($title) && empty($categoryId) && empty($country)){
        $searchedJobs = \App\Job::where('status', 'Active')->where('title', 'like', '%'.$title.'%')
        ->get();
      }
      else if(empty($title) && !empty($categoryId) && empty($country)){
        $searchedJobs = \App\Job::where('status', 'Active')->where('categoryId', $categoryId)->get();
      }
      else if(!empty($title) && empty($categoryId) && !empty($country)){
        $searchedJobs = \App\Job::where('status', 'Active')->where('country', $country)
        ->where('title', 'like', '%'.$title.'%')->get();
      }
      else if(!empty($title) && !empty($categoryId) && empty($country)){
        $searchedJobs = \App\Job::where('status', 'Active')->where('categoryId', $categoryId)
        ->where('title', 'like', '%'.$title.'%')->get();
      }
      else if(empty($title) && empty($categoryId) && !empty($country)){

        $searchedJobs = \App\Job::where('status', 'Active')->where('country', $country)->get();
      }
      return view('front.jobs', ['searchedJobs' => $searchedJobs]);
    }

    public function jobsOfCategory($categoryId){
      $jobs = \App\Job::where('status', 'Active')->where('categoryId', $categoryId)->get();
      return view('front.jobs', ['searchedJobs' => $jobs]);
    }
}
