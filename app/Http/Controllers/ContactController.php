<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.contacts');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contacts = \App\Contact::where('status', 'Active')->where('type', 'Primary')->get();
        if($contacts->isEmpty()){
            $contact = new \App\Contact;
            $contact->contact = $request->get('contact');
            $contact->title = $request->get('title');
            $contact->type = $request->get('type');
            $contact->status = 'Active';
            $contact->save();
        }
        else{
            if($request->get('type') == 'Primary'){
                foreach ($contacts as $key => $contact) {
                    $contact->type = 'Secondary';
                    $contact->save();
                }
            }
            $contact = new \App\Contact;
            $contact->contact = $request->get('contact');
            $contact->type = $request->get('type');
            $contact->title = $request->get('title');
            $contact->status = 'Active';
            $contact->save();
        }
        return redirect('contacts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = \App\Contact::find($id);
        return view('dashboard.contacts')->with('contact', $contact);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = \App\Contact::find($id);
        $contacts = \App\Contact::where('id', '!=', $id)->get();
        if($contacts->isEmpty()){
            $contact->contact = $request->get('contact');
            $contact->title = $request->get('title');
            $contact->type = $request->get('type');
            $contact->status = 'Active';
            $contact->save();
        }
        else{
            if($request->get('type') == 'Primary'){
                foreach ($contacts as $key => $value) {
                    $value->type = 'Secondary';
                    $value->save();
                }
            }
            $contact->contact = $request->get('contact');
            $contact->type = $request->get('type');
            $contact->title = $request->get('title');
            $contact->status = 'Active';
            $contact->save();
        }
        return redirect('contacts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = \App\Contact::find($id);
        $contact->delete();
        return redirect('contacts');
    }

    public function changestatus($id){
        $contact = \App\Contact::find($id);
        if($contact->status == 'Active'){
            $contact->status = 'Deactive';
        }
        else{
            $contact->status = 'Active';
        }
        $contact->save();
        return redirect('contacts');
      }
}
