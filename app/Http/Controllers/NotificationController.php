<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.notifications');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notification = new \App\Notification;
        $notification->useage = $request->get('useage');
        $notification->message = $request->get('message');
        $notification->status = 'Active';
        $notification->save();
        $notifications = \App\Notification::where('status', 'Active')->where('useage', $notification->useage)->where('id', '!=', $notification->id)->get();
        foreach ($notifications as $key => $value) {
          $value->status = 'Deactive';
          $value->save();
        }
        return redirect('notifications');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notification = \App\Notification::find($id);
        return view('dashboard.notifications', ['notification' => $notification]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $notification = \App\Notification::find($id);
      $notification->useage = $request->get('useage');
      $notification->message = $request->get('message');
      $notification->save();
      if($notification->status == 'Active'){
        $notifications = \App\Notification::where('status', 'Active')->where('useage', $notification->useage)->where('id', '!=', $id)->get();
        foreach ($notifications as $key => $value) {
          $value->status = 'Deactive';
          $value->save();
        }
      }
      return redirect('notifications');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notification = \App\Notification::find($id);
        $notification->delete();
        return redirect('notifications');
    }

    public function changestatus($id){
        $notification = \App\Notification::find($id);
        if($notification->status == 'Active'){
          $notification->status = 'Deactive';
        }
        else{
          $notification->status = 'Active';
        }
        $notification->save();
        if($notification->status == 'Active'){
          $notifications = \App\Notification::where('status', 'Active')->where('useage', $notification->useage)->where('id', '!=', $id)->get();
          foreach ($notifications as $key => $value) {
            $value->status = 'Deactive';
            $value->save();
          }
        }
        return redirect('notifications');
    }
}
