<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.socials');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $social = new \App\Social;
        $social->link = $request->get('link');
        $social->icon = $request->get('icon');
        $social->status = 'Active';
        $social->save();
        return redirect('socials');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $social = \App\Social::find($id);
        return view('dashboard.socials', ['social' => $social]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $social = \App\Social::find($id);
        $social->link = $request->get('link');
        $social->icon = $request->get('icon');
        $social->save();
        return redirect('socials');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $social = \App\Social::find($id);
        $social->delete();
        return redirect('socials');
    }

    public function changestatus($id){
        $social = \App\Social::find($id);
        if($social->status == 'Active'){
          $social->status = 'Deactive';
        }
        else{
          $social->status = 'Active';
        }
        $social->save();
        return redirect('socials');
    }
}
