<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.stats');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stat = new \App\Stat;
        $stat->title = $request->get('title');
        $stat->number = $request->get('number');
        $stat->status = 'Active';
        $stat->save();
        return redirect('stats');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stat = \App\Stat::find($id);
        return view('dashboard.stats')->with('stat', $stat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stat = \App\Stat::find($id);
        $stat->title = $request->get('title');
        $stat->number = $request->get('number');
        $stat->save();
        return redirect('stats');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stat = \App\Stat::find($id);
        $stat->delete();
        return redirect('stats');
    }

    public function changestatus($id){
        $stat = \App\Stat::find($id);
        if($stat->status == 'Active'){
          $stat->status = 'Deactive';
        }
        else{
          $stat->status = 'Active';
        }
        $stat->save();
        return redirect('stats');
    }
}
