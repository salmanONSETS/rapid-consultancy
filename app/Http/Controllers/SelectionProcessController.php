<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class SelectionProcessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.selectionProcesses');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $selectionProcess = new \App\SelectionProcess;
      $selectionProcess->title = $request->get('title');
      $selectionProcess->detail = $request->get('detail');
      $selectionProcess->serialNumber = $request->get('serialNumber');
      $selectionProcess->status = 'Active';

      if($request->hasFile('image')){
        $destinationImages='selectionProcessesData/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $file->move($destinationImages,$filename);
          $selectionProcess->picturePath = $filename;
        }
        else{
          $message = 'Only .png, .jpg, .jpeg accepted';
          return redirect('selectionProcesses')->with('message', $message);
        }
      }

      $selectionProcess->save();
      return redirect('selectionProcesses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $selectionProcess = \App\SelectionProcess::find($id);
      return view('dashboard.selectionProcesses', ['selectionProcess' => $selectionProcess]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $selectionProcess = \App\SelectionProcess::find($id);
      $selectionProcess->title = $request->get('title');
      $selectionProcess->detail = $request->get('detail');
      $selectionProcess->serialNumber = $request->get('serialNumber');

      if($request->hasFile('image')){
        $destinationImages='selectionProcessesData/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        if(!empty($selectionProcess->picturePath)){
          $path = 'selectionProcessesData/images/'.$selectionProcess->picturePath;
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $selectionProcess->picturePath = $filename;
          }
          File::delete($path);
        }
        else{
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $file->move($destinationImages,$filename);
            $selectionProcess->picturePath = $filename;
          }
          else{
            $message = 'Only .png, .jpg, .jpeg accepted';
            return redirect('selectionProcesses')->with('message', $message);
          }
        }
      }

      $selectionProcess->save();
      return redirect('selectionProcesses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $selectionProcess = \App\SelectionProcess::find($id);
        if(!empty($selectionProcess->picturePath)){
          $path = 'selectionProcessesData/images/'.$selectionProcess->picturePath;
          File::delete($path);
        }
        $selectionProcess->delete();
        return redirect('selectionProcesses');
    }

    public function changestatus($id){
      $selectionProcess = \App\SelectionProcess::find($id);
      if($selectionProcess->status == 'Active'){
        $selectionProcess->status = 'Deactive';
      }
      else{
        $selectionProcess->status = 'Active';
      }
      $selectionProcess->save();
      return redirect('selectionProcesses');
    }
}
