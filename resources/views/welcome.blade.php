<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Rapid Overseas Employment Promoters</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">

    <meta name="keywords" content="Rapid Overseas Employment Promoters, Employment, Overseas" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <link rel="icon" href="{{ asset('./img/logo.png') }}" type="image" sizes="16x16">
    <link href="{{ asset('css/front/bootstrap.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ asset('css/front/zoomslider.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ asset('css/front/style6.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ asset('css/front/style.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ asset('css/front/fontawesome-all.css') }}" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
</head>

<body>
    <!-- banner-inner -->
    <div id="demo-1" data-zs-overlay="dots" data-zs-src='["{{asset('imagesData/images/1.jpg')}}", "{{asset('imagesData/images/2.jpg')}}", "{{asset('imagesData/images/3.jpg')}}", "{{asset('imagesData/images/4.jpg')}}"]'>
        <div class="demo-inner-content">
            <div class="header-top">
                <header>
                    <div class="top-head ml-lg-auto text-center">
                        <div class="row">
                            <div class="col-md-6">
                                <span>Welcome Back!</span>
                            </div>
                            <div class="col-md-6 sign-btn">
                                <a href="#" data-toggle="modal" data-target="#exampleModalCenter">
                                    <i class="fas fa-lock"></i> Sign In
                                </a>
                            </div>
                            <div class="col-md-3 sign-btn" hidden>
                                <a href="#" data-toggle="modal" data-target="#exampleModalCenter2">
                                    <i class="far fa-user"></i> Register</a>
                            </div>
                            <div class="search col-md-2" hidden>
                                <div class="mobile-nav-button">
                                    <button id="trigger-overlay" type="button">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                                <!-- open/close -->
                                <div class="overlay overlay-door">
                                    <button type="button" class="overlay-close">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                    <form action="#" method="post" class="d-flex">
                                        <input class="form-control" type="search" placeholder="Search here..." required="">
                                        <button type="submit" class="btn btn-primary submit">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </form>
                                </div>
                                <!-- open/close -->
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <div class="logo">
                            <h1>
                                <a class="navbar-brand" href="index.html">
                                    <img src="{{ asset('./img/logo.png') }}" width="30" height="30" style="width:30px" /> Rapid OEP
                                </a>
                            </h1>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon">
                                <i class="fas fa-bars"></i>
                            </span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-lg-auto text-center">
                            <?php
                            $name = Route::currentRouteName();
                            ?>
                                <li @if($name) === '/' class="nav-item active" @else class="nav-item" @endif>
                                    <a class="nav-link" href="/">Home
                                        <span class="sr-only">(current)</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/viewJobs">Jobs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/selectionProcess">Selection Process</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/ourProjects">Our Projects</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/viewTeam">Team</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/contactUs">Contact Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/aboutUs">About</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>
            </div>
            <!--/banner-info-w3layouts-->
            <div class="banner-info-w3layouts text-center">
                <h3>
                    <span>Find the Right Job</span> .
                    <span>Right Now.</span>
                </h3>
                <p>Your job search starts and ends with us.</p>
                <form action="/searchJobs" method="post" class="ban-form row">
                  {{ csrf_field() }}
                    <div class="col-md-3 banf">
                        <input class="form-control" type="text" name="title" placeholder="Title">
                    </div>
                    <div class="col-md-3 banf">
                        <select class="form-control" id="country12" name="country">
                            <option>Select Country</option>
                            <?php
                            $countries = array();
                            $jobs = \App\Job::where('status', 'Active')->get();
                            foreach ($jobs as $key => $value) {
                              if(!in_array($value->country, $countries)){
                                array_push($countries, $value->country);
                              }
                            }
                            ?>
                            @foreach($countries as $country)
                            <option>{{$country}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 banf">
                      <?php
                      $categories = \App\Category::where('status', 'Active')->get();
                      ?>
                        <select id="country13" class="form-control" name="categoryId">
                          <option value="">Select Category</option>
                          @foreach($categories as $category)
                          <option value="{{$category->id}}">{{$category->name}}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 banf">
                        <button class="btn1" type="submit">FIND JOB
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </form>
            </div>
            <!--//banner-info-w3layouts-->
        </div>
    </div>
    <!-- banner-text -->
    <!-- banner-bottom-wthree -->
    <section class="banner-bottom-wthree py-lg-5 py-md-5 py-3">
        <div class="container">
            <div class="inner-sec-w3ls py-lg-5  py-3">
                <h3 class="tittle text-center mb-lg-4 mb-3">
                    <span>Our Mission</span>Popular Categories
                </h3>
                <div class="row populor_category_grids mt-5 justify-content-center">
                  <?php
                  $categories = \App\Category::where('status', 'Active')->get();
                  ?>
                  @foreach($categories as $category)
                  <div class="col-md-3 category_grid">
                      <div class="view view-tenth">
                          <div class="category_text_box">
                              {!! $category->icon !!}
                              <h3> {{$category->name}}</h3>
                              <?php
                              $count = \App\Job::where('status', 'Active')->where('categoryId', $category->id)->count();
                              ?>
                              <p>({{$count}} open flow-positions)</p>
                          </div>
                          <div class="mask">
                              <a href="/categoryJobs/{{$category->id}}">
                                <img src="{{asset('categoriesData/images/').'/'.$category->picturePath}}" alt="Category Image" class="img-fluid" />
                              </a>
                          </div>
                      </div>
                  </div>
                  @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- //banner-bottom-wthree -->
    <!--/process-->
    <section class="banner-bottom-wthree pb-lg-5 pb-md-4 pb-3">
        <div class="container">
            <div class="inner-sec-w3ls py-lg-5  py-3">
			<!---728x90--->
                <h3 class="tittle text-center mb-lg-4 mb-3">
                    <span>Some Info</span>Latest Job flow-positions</h3>
					<!---728x90--->
                <div class="tabs mt-5">
                    <ul class="nav nav-pills my-4" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Featured Jobs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Recent Jobs</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="menu-grids mt-4">
                                <div class="row t-in">
                                    <div class="col-lg-8 text-info-sec">
                                        <?php
                                        $featuredJobs = \App\Job::where('status', 'Active')->orderBy('created_at', 'desc')->where('featured', 1)->get();
                                        ?>
                                        @foreach($featuredJobs as $featuredJob)
                                        <div class="job-post-main row mb-3">
                                            <div class="col-md-9 job-post-info text-left">
                                                <div class="job-post-icon">
                                                    <i class="fas fa-briefcase"></i>
                                                </div>
                                                <div class="job-single-sec">
                                                    <h4>
                                                        <a href="#">{{$featuredJob->title}}</a>
                                                    </h4>
                                                    <p class="my-2">{{$featuredJob->companyName}}</p>
                                                    <ul class="job-list-info d-flex">
                                                        <li>
                                                          <?php
                                                          $category = \App\Category::find($featuredJob->categoryId);
                                                          ?>
                                                            <i class="fas fa-briefcase"></i> {{$category->name}}</li>
                                                        <li>
                                                            <i class="fas fa-map-marker-alt"></i> {{$featuredJob->country}}</li>
                                                        <li>
                                                            <i class="fas fa-dollar-sign"></i> {{$featuredJob->salary}}</li>
                                                    </ul>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-3 job-single-time text-right">
                                                <span class="job-time">
                                                <a href="/apply/{{$featuredJob->id}}" class="aply-btn ">Appy Now</a>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="col-lg-4 text-info-sec">
                                      <?php
                                      $rightImage = \App\JobSideImage::where('status', 'Active')->where('direction', 'Right')->first();
                                      ?>
                                      @if(empty($rightImage))
                                      <img src="{{ asset('img/job-1.jpg') }}" alt="Job Side Image" class="img-fluid" />
                                      @else
                                      <img src="{{ asset('jobSideImagesData/images/').'/'.$rightImage->picturePath }}" alt="Job Side Image" class="img-fluid">
                                      @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="menu-grids mt-4">
                                <div class="row t-in">
                                    <div class="col-lg-4 text-info-sec">
                                      <?php
                                      $leftImage = \App\JobSideImage::where('status', 'Active')->where('direction', 'Left')->first();
                                      ?>
                                      @if(empty($leftImage))
                                      <img src="{{ asset('img/job-2.jpg') }}" alt="Job Side Image" class="img-fluid" />
                                      @else
                                      <img src="{{ asset('jobSideImagesData/images/').'/'.$leftImage->picturePath }}" alt="Job Side Image" class="img-fluid">
                                      @endif
                                    </div>
                                    <div class="col-lg-8 text-info-sec">
                                        <?php
                                        $recentJobs = \App\Job::where('status', 'Active')->orderBy('created_at', 'desc')->get();
                                        ?>
                                        @foreach($recentJobs as $recentJob)
                                        <div class="job-post-main row mb-3">
                                            <div class="col-md-9 job-post-info text-left">
                                                <div class="job-post-icon">
                                                    <i class="fas fa-briefcase"></i>
                                                </div>
                                                <div class="job-single-sec">
                                                    <h4>
                                                        <a href="#">{{$recentJob->title}}</a>
                                                    </h4>
                                                    <p class="my-2">{{$recentJob->companyName}}</p>
                                                    <ul class="job-list-info d-flex">
                                                        <li>
                                                          <?php
                                                          $category = \App\Category::find($recentJob->categoryId);
                                                          ?>
                                                            <i class="fas fa-briefcase"></i> {{$category->name}}
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-map-marker-alt"></i> {{$recentJob->country}}
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-dollar-sign"></i> {{$recentJob->salary}}
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-3 job-single-time text-right">
                                                <span class="job-time">
                                                <a href="/apply/{{$recentJob->id}}" class="aply-btn ">Appy Now</a>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--//preocess-->

    <!--job -->
    <section class="banner-bottom-wthree py-lg-5 py-md-5 py-3">
        <div class="container">
            <div class="inner-sec-w3ls py-lg-5  py-3">
                <h3 class="tittle text-center mb-lg-5 mb-3">
                    <span>Some Info</span>Selection Process
                  </h3>
                <div class="mid-info text-center mt-5">
                    <div class="parent-chart">
                        <div class="level lev-one top-level">
                            <div class="flow-position">
                                <img src="{{ asset('img/s1.jpg') }}" alt=" " class="img-fluid rounded-circle">
                                <br>
                                <strong>Recruitment Process</strong>
                                <br> Lorem ipsum
                            </div>
                        </div>
                        <div class="flow-chart">
                            <div class="level lev-two last-lev">
                              <?php
                              $selectionProcesses = \App\SelectionProcess::where('status', 'Active')->orderBy('serialNumber', 'asc')->get();
                              ?>
                              @foreach($selectionProcesses as $selectionProcess)
                              <div class="flow-position">
                                <img src="{{ asset('selectionProcessesData/images/').'/'.$selectionProcess->picturePath }}" alt="Selection Process Image" class="img-fluid rounded-circle">
                                  <br>
                                  <strong>{{$selectionProcess->title}}</strong>
                                  <br> {{$selectionProcess->detail}}
                              </div>
                              @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--//job -->
    <!--/candidates -->
    <section class="banner-bottom-wthree bg-light py-lg-5 py-3 text-center">
        <div class="container">
            <div class="inner-sec-w3ls py-lg-4 py-md-4 py-3">
                <h3 class="tittle text-center mb-lg-5 mb-3">
                    <span>Some Info</span>Team
                </h3>
                <div class="row mt-5">
                  <?php
                  $team = \App\Team::where('status', 'Active')->get();
                  ?>
                  @foreach($team as $teamMember)
                  <div class="col-lg-3 member-main text-center">
                      <div class="card">
                          <div class="card-body">
                              <div class="member-img">
                                <img src="{{asset('teamsData/images/').'/'.$teamMember->picturePath}}" alt="Team Member Image" class="img-fluid rounded-circle" />
                              </div>
                              <div class="member-info text-center py-lg-4 py-2">
                                  <h4>{{$teamMember->name}}</h4>
                                  <p class="my-4"> {{$teamMember->designation}}</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  @endforeach
                </div>
            </div>
        </div>
    </section>
    <!--/candidates -->
    <!--/stats-->
    <section class="banner-bottom-wthree bg-dark dotts py-lg-5 py-3">
        <div class="container">
            <div class="inner-sec-w3ls py-lg-5  py-3">
                <h3 class="tittle cen text-center mb-lg-5 mb-3">
                    <span>Some Info</span> Our Stats
                </h3>
                <div class="stats row mt-5">
                    <?php
                    $stats = \App\Stat::where('status', 'Active')->get();
                    ?>
                    @foreach($stats as $value)
                    <div class="col-md-3 stats_left counter_grid text-center">
                        <p class="counter">{{$value->number}}</p>
                        <h4>{{$value->title}}</h4>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!--//stats-->

    <!--job -->
    <section class="banner-bottom-wthree py-lg-5 py-md-5 py-3">
        <div class="container">
            <div class="inner-sec-w3ls py-lg-5  py-3">
                <h3 class="tittle text-center mb-lg-5 mb-3">
                    <span>Some Info</span> Quick Career Tips
                </h3>
                <div class="row mt-5">
                    <div class="card-deck">
                      <?php
                      $careerTips = \App\CareerTip::where('status', 'Active')->get();
                      ?>
                      @foreach($careerTips as $careerTip)
                      <div class="card">
                        <img src="{{asset('careerTipsData/images/').'/'.$careerTip->picturePath}}" alt="Career Tip Image" class="img-fluid card-img-top" />
                          <div class="card-body">
                              <h5 class="card-title">{{$careerTip->title}}</h5>
                              <p class="card-text">{{$careerTip->detail}}</p>
                          </div>
                      </div>
                      @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--//job -->

    <!--model-forms-->
    <!--/Login-->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="login px-4 mx-auto mw-100">
                        <h5 class="text-center mb-4">Login Now</h5>
                        <form action="{{ url('/login') }}" method="post">
                          {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="mb-2">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" placeholder="Email" required  value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="mb-2">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-check mb-2">
                                <input type="checkbox" name="remember" class="form-check-input" id="exampleCheck1"{{ old('remember') ? 'checked' : ''}}>
                                <label class="form-check-label" for="exampleCheck1">Remember Me</label>
                            </div>
                            <button type="submit" class="btn btn-primary submit mb-4">Sign In</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--//Login-->
    <!--/Register-->
    <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="login px-4 mx-auto mw-100">
                        <h5 class="text-center mb-4">Register Now</h5>
                        <form action="#" method="post">
                            <div class="form-group">
                                <label>First name</label>
                                <input type="text" class="form-control" id="validationDefault01" placeholder="" required="">
                            </div>
                            <div class="form-group">
                                <label>Last name</label>
                                <input type="text" class="form-control" id="validationDefault02" placeholder="" required="">
                            </div>
                            <div class="form-group">
                                <label class="mb-2">Password</label>
                                <input type="password" class="form-control" id="password1" placeholder="" required="">
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" class="form-control" id="password2" placeholder="" required="">
                            </div>
                            <button type="submit" class="btn btn-primary submit mb-4">Register</button>
                            <p class="text-center pb-4">
                                <a href="#">By clicking Register, I agree to your terms</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--//Register-->

    <!--//model-form-->

    <!--footer -->
    <footer class="footer-emp-w3layouts bg-dark dotts py-lg-5 py-3">
        <div class="container-fluid px-lg-5 px-3">
            <div class="row footer-top">
                <div class="col-lg-4 footer-grid-wthree-w3ls">
                    <div class="footer-title">
                        <h3>About Us</h3>
                    </div>
                    <?php
                    $about = \App\About::where('status', 'Active')->first();
                    ?>
                    <div class="footer-text">
                        @if(!empty($about))
                        <p>{{$about->detail}}</p>
                        @endif
                        <ul class="footer-social text-left mt-lg-4 mt-3">
                            <?php
                            $socials = \App\Social::where('status', 'Active')->get()
                            ?>
                            @foreach($socials as $value)
                            <li class="mx-2">
                                <a href="{{$value->link}}" target="_blank">
                                    {!! $value->icon !!}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 footer-grid-wthree-w3ls">
                    <div class="footer-title">
                        <h3>Get in touch</h3>
                    </div>
                    <div class="contact-info">
                        <h4>Location :</h4>
                        <?php
                        $address = \App\Address::where('status', 'Active')->where('type', 'Primary')->first();
                        $contact = \App\Contact::where('status', 'Active')->where('type', 'Primary')->first();
                        $email = \App\Email::where('status', 'Active')->Where('type', 'Primary')->first();
                        ?>
                        @if(!empty($address))
                        <p>{{$address->address}}</p>
                        @endif
                        <div class="phone">
                            <h4>Contact :</h4>
                            <p>Phone : @if(!empty($contact)) {{$contact->contact}} @endif</p>
                            <p>Email :
                                @if(!empty($email))
                                <a href="mailto:info@example.com">{{$email->email}}</a>
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 footer-grid-wthree-w3ls">
                    <div class="footer-title">
                        <h3>Quick Links</h3>
                    </div>
                    <ul class="links">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="/viewJobs">Jobs</a>
                        </li>
                        <li>
                            <a href="/selectionProcess">Selection Process</a>
                        </li>
                        <li>
                            <a href="/ourProjects">Our Projects</a>
                        </li>
                        <li>
                            <a href="/viewTeam">Team</a>
                        </li>
                    </ul>
                    <ul class="links">
                        <li>
                            <a href="/contactUs">Contact Us</a>
                        </li>
                        <li>
                            <a href="/aboutUs">About Us</a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="copyright mt-4">
                <p class="copy-right text-center" id="copyright">&copy;
                    <script>
                        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                    </script> Replenish. All Rights Reserved | Design by
                    <a href="http://w3layouts.com/"> W3layouts </a> | Developed by
                    <a href="http://www.onsets.co/" target="_blank"> ONSETS </a>
                </p>
            </div>
        </div>
    </footer>
    <!-- //footer -->
    <script src="{{ asset('js/front/jquery-1.11.1.min.js') }}"></script>
        <script src="{{ asset('js/front/modernizr-2.6.2.min.js') }}"></script>
        <script src="{{ asset('js/front/jquery.zoomslider.min.js') }}"></script>
        <!--//slider-->
        <!--search jQuery-->
        <script src="{{ asset('js/front/classie-search.js') }}"></script>
        <script src="{{ asset('js/front/demo1-search.js') }}"></script>
        <!--//search jQuery-->

        <script>
            $(document).ready(function() {
                $(".dropdown").hover(
                    function() {
                        $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                        $(this).toggleClass('open');
                    },
                    function() {
                        $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                        $(this).toggleClass('open');
                    }
                );
            });
        </script>
        <!-- //dropdown nav -->
        <!-- password-script -->
        <script>
            window.onload = function() {
                document.getElementById("password1").onchange = validatePassword;
                document.getElementById("password2").onchange = validatePassword;
            }

            function validatePassword() {
                var pass2 = document.getElementById("password2").value;
                var pass1 = document.getElementById("password1").value;
                if (pass1 != pass2)
                    document.getElementById("password2").setCustomValidity("Passwords Don't Match");
                else
                    document.getElementById("password2").setCustomValidity('');
                //empty string means no validation error
            }
        </script>
        <!-- //password-script -->

        <!-- stats -->
        <script src="{{ asset('js/front/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('js/front/jquery.countup.js') }}"></script>
        <script>
            $('.counter').countUp();
        </script>
        <!-- //stats -->

        <!-- //js -->
        <script src="{{ asset('js/front/bootstrap.js') }}"></script>
        <!--/ start-smoth-scrolling -->
        <script src="{{ asset('js/front/move-top.js') }}"></script>
        <script src="{{ asset('js/front/easing.js') }}"></script>
        <script>
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event) {
                    event.preventDefault();
                    $('html,body').animate({
                        scrollTop: $(this.hash).offset().top
                    }, 900);
                });
            });
        </script>
        <script>
            $(document).ready(function() {
                /*
                						var defaults = {
                							  containerID: 'toTop', // fading element id
                							containerHoverID: 'toTopHover', // fading element hover id
                							scrollSpeed: 1200,
                							easingType: 'linear'
                						 };
                						*/

                $().UItoTop({
                    easingType: 'easeOutQuart'
                });

            });
        </script>
        <!--// end-smoth-scrolling -->
        <!-- bootstrap notify -->
      <script src="{{ asset('./js/front/bootstrap-notify.js') }}" type="text/javascript"></script>
      @if(session('message'))
      <script type="text/javascript">
      $(document).ready(function(){
        $.notify({
          message: "{{session('message')}}"
        },
        {
          type: 'success',
          allow_dismiss: true,
          timer: 4000
        });
      });
      </script>
      @endif
</body>

</html>
