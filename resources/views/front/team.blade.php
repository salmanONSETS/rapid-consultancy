@extends('layouts.app')
@section('content')
<ol class="breadcrumb justify-content-left">
  <li class="breadcrumb-item">
    <a href="/">Home</a>
  </li>
  <li class="breadcrumb-item active">Team</li>
</ol>
<section class="banner-bottom-wthree bg-light py-lg-5 py-3 text-center">
  <div class="container">
    <div class="inner-sec-w3ls py-lg-4 py-md-4 py-3">
      <h3 class="tittle text-center mb-lg-5 mb-3">
        <span>Some Info</span>Team
      </h3>
      <div class="row mt-5">
        <?php
        $team = \App\Team::where('status', 'Active')->get();
        ?>
        @foreach($team as $teamMember)
        <div class="col-lg-3 member-main text-center">
          <div class="card">
            <div class="card-body">
              <div class="member-img">
                <img src="{{asset('teamsData/images/').'/'.$teamMember->picturePath}}" alt="Team Member Image" class="img-fluid rounded-circle" />
              </div>
              <div class="member-info text-center py-lg-4 py-2">
                <h4>{{$teamMember->name}}</h4>
                <p class="my-4"> {{$teamMember->designation}}</p>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
@endsection
