@extends('layouts.app')
@section('content')
<ol class="breadcrumb justify-content-left">
    <li class="breadcrumb-item">
        <a href="/">Home</a>
	</li>
	<li class="breadcrumb-item active">Apply</li>
</ol>
<!-- banner-text -->
<!-- contact -->
<section class="banner-bottom-wthree pt-lg-5 pt-md-3 pt-3 pb-lg-5 pb-md-3 pb-3">
	<div class="inner-sec-w3ls pt-md-5 pt-md-3 pt-3">
		<h3 class="tittle text-center mb-lg-5 mb-3">
      Apply
    </h3>
		<div class="container px-md-5">
      <div class="row px-md-5">
        <div class="col-md-12">
          <form method="post" action="/applyForJob" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="text" value="{{$jobId}}" name="jobId" hidden />
            <div class="row">
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Name</label>
                      <input type="text" name="name" placeholder="Name" class="form-control" />
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Phone</label>
                      <input type="tel" name="phone" placeholder="Phone" class="form-control" />
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Email</label>
                      <input type="email" name="email" placeholder="Email" class="form-control" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Comments</label>
                  <textarea name="comments" rows="8" placeholder="Comments" class="form-control"></textarea>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <div class="form-inputs">
                    <p>Specify your current employment status</p>
                    <div class="d-flex justify-content-between">
                      <div>
                        <input type="radio" id="a-option" name="jobStatus" value="Employed">
                        <label for="a-option">Employed </label>
                      </div>
                      <div>
                        <input type="radio" id="b-option" name="jobStatus" value="Unemployed">
                        <label for="b-option">Unemployed</label>
                      </div>
                      <div>
                        <input type="radio" id="c-option" name="jobStatus" value="Self-Employed">
                        <label for="c-option">Self-Employed</label>
                      </div>
                      <div>
                        <input type="radio" id="d-option" name="jobStatus" value="Student">
                        <label for="d-option">Student</label>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 text-center">
                <div class="form-inputs upload" style="position:relative">
                  <p>Upload your resume</p>
                  <input type="file" name="file" style="opacity:0; position:absolute; left:34%" accept=".pdf,.doc,.docx" required>
                  <button id="filedrag" style="display:inline-block" class="btn btn-lg">Upload</button>
                </div>
              </div>
              <input type="submit" class="btn btn-lg btn-success btn-block mt-5" value="Submit CV" />
            </div>
          </form>
        </div>
      </div>
		</div>
	</div>
</section>
@endsection
