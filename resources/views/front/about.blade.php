@extends('layouts.app')
@section('content')
<ol class="breadcrumb justify-content-left">
    <li class="breadcrumb-item">
        <a href="/">Home</a>
    </li>
    <li class="breadcrumb-item active">About</li>
</ol>
<!-- banner-text -->
<section class="banner-bottom-wthree bg-light py-lg-5 py-3">
    <div class="container">
        <div class="inner-sec-w3ls py-md-5 py-3">
            <h3 class="tittle text-center mb-lg-5 mb-3">
                <span>Overview</span>About Us
            </h3>        
            <div class="candidate-ab-info mt-5 row">
                <div class="col-lg-8 about-in-details">
                    <?php
                    $about = \App\About::where('status', 'Active')->first();
                    ?>
                    @if(!empty($about))
                    <h5 class="j-b mb-3">{{$about->title}}</h5>
                    <p>{{$about->detail}}</p>
                    @endif
                </div>
                <div class="col-lg-4 about-in-img">
                    @if(empty($about->picturePath))
                    <img src="{{ asset('img/g8.jpg') }}" alt="About image" class="img-fluid">
                    @else
                    <img src="{{ asset('aboutpage/images/').'/'.$about->picturePath }}" alt="About Image" class="img-fluid">
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!--/stats-->
<section class="banner-bottom-wthree bg-dark dotts py-lg-5 py-3">
    <div class="container">
        <div class="inner-sec-w3ls py-lg-5  py-3">
            <h3 class="tittle cen text-center mb-lg-5 mb-3">
                <span>Some Info</span> Our Stats
            </h3>
            <div class="stats row mt-5">
                <?php
                $stats = \App\Stat::where('status', 'Active')->get();
                ?>
                @foreach($stats as $value)
                <div class="col-md-3 stats_left counter_grid text-center">
                    <p class="counter">{{$value->number}}</p>
                    <h4>{{$value->title}}</h4>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!--//stats-->
<!-- plans -->
<section class="banner-bottom-wthree bg-light py-lg-5 py-3">
    <div class="container">
        <div class="inner-sec-w3ls py-md-5 py-3">
            <h3 class="tittle text-center mb-lg-5 mb-3">
                <span>We offer</span>Our Services
            </h3>
            <div class="row mt-5">
                <?php
                $services = \App\Service::where('status', 'Active')->get();
                ?>
                @foreach($services as $value)
                <div class="serve-grid col-md-4 col-sm-6 mt-lg-5 text-center">
                    {!! $value->icon !!}
                    <h4 class="my-lg-4 my-3">{{$value->title}}</h4>
                    <p>{{$value->detail}}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- //plans -->
@endsection