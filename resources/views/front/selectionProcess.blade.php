@extends('layouts.app')
@section('content')
<ol class="breadcrumb justify-content-left">
  <li class="breadcrumb-item">
    <a href="/">Home</a>
  </li>
  <li class="breadcrumb-item active">Selection Process</li>
</ol>
<section class="banner-bottom-wthree py-lg-5 py-md-5 py-3">
  <div class="container">
    <div class="inner-sec-w3ls py-lg-5  py-3">
      <h3 class="tittle text-center mb-lg-4 mb-3">
        <span>Some Info</span>How It works
      </h3>
      <?php
      $hows = \App\How::where('status', 'Active')->orderBy('serialNumber', 'asc')->get();
      $count = 0;
      ?>
      @foreach($hows as $how)
      @if($count%2 === 0)
      <div class="row choose-main mt-5">
        <div class="col-lg-6 work-grid-right">
          <div class="work-info">
            <h5>
              <span class="post-color">Step : {{$how->serialNumber}}</span>
            </h5>
            <h4 class="post my-3">{{$how->title}}</h4>
            <p>{{$how->detail}}</p>
          </div>
        </div>
        <div class="col-lg-6 work-grid-left">
          <img src="{{asset('howsData/images/').'/'.$how->picturePath}}" alt="How It Works Image" class="img-fluid" />
        </div>
      </div>
      @else
      <div class="row mt-5">
        <div class="col-lg-6 work-grid-left">
          <img src="{{asset('howsData/images/').'/'.$how->picturePath}}" alt="How It Works Image" class="img-fluid" />
        </div>
        <div class="col-lg-6 work-grid-right">
          <div class="work-info">
            <h5>
              <span class="post-color">Step : {{$how->serialNumber}}</span>
            </h5>
            <h4 class="post my-3">{{$how->title}}</h4>
            <p>{{$how->detail}}</p>
          </div>
        </div>
      </div>
      @endif
      <?php $count++; ?>
      @endforeach
    </div>
  </div>
</section>
<section class="banner-bottom-wthree py-lg-5 py-md-5 py-3">
  <div class="container">
    <div class="inner-sec-w3ls py-lg-5  py-3">
      <h3 class="tittle text-center mb-lg-4 mb-3">
        <span>Our Mission</span>Popular Categories
      </h3>
      <div class="row populor_category_grids mt-5 justify-content-center">
        <?php
        $categories = \App\Category::where('status', 'Active')->get();
        ?>
        @foreach($categories as $category)
        <div class="col-md-3 category_grid">
          <div class="view view-tenth">
            <div class="category_text_box">
              {!! $category->icon !!}
              <h3> {{$category->name}}</h3>
              <?php
              $count = \App\Job::where('status', 'Active')->where('categoryId', $category->id)->count();
              ?>
              <p>({{$count}} open flow-positions)</p>
            </div>
            <div class="mask">
              <a href="#">
                <img src="{{asset('categoriesData/images/').'/'.$category->picturePath}}" alt="Category Image" class="img-fluid" />
              </a>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
<section class="banner-bottom-wthree py-lg-5 py-md-5 py-3">
  <div class="container">
    <div class="inner-sec-w3ls py-lg-5  py-3">
      <h3 class="tittle text-center mb-lg-5 mb-3">
        <span>Some Info</span>Selection Process
      </h3>
      <div class="mid-info text-center mt-5">
        <div class="parent-chart">
          <div class="level lev-one top-level">
            <div class="flow-position">
              <img src="{{ asset('img/s1.jpg') }}" alt=" " class="img-fluid rounded-circle">
              <br>
              <strong>Recruitment Process</strong>
              <br> Lorem ipsum
            </div>
          </div>
          <div class="flow-chart">
            <div class="level lev-two last-lev">
              <?php
              $selectionProcesses = \App\SelectionProcess::where('status', 'Active')->orderBy('serialNumber', 'asc')->get();
              ?>
              @foreach($selectionProcesses as $selectionProcess)
              <div class="flow-position">
                <img src="{{ asset('selectionProcessesData/images/').'/'.$selectionProcess->picturePath }}" alt="Selection Process Image" class="img-fluid rounded-circle">
                <br>
                <strong>{{$selectionProcess->title}}</strong>
                <br> {{$selectionProcess->detail}}
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
