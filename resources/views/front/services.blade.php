@extends('layouts.app')
@section('content')
<ol class="breadcrumb justify-content-left">
  <li class="breadcrumb-item">
    <a href="/">Home</a>
  </li>
  <li class="breadcrumb-item active">Services</li>
</ol>
<section class="banner-bottom-wthree py-lg-5 py-md-5 py-3">
  <div class="container">
    <div class="inner-sec-w3ls py-lg-5 py-md-5  py-3">
      <h3 class="tittle text-center mb-lg-5 mb-3">
        <span>We offer</span>Our Services
      </h3>
      <div class="row mt-5">
        <?php
        $services = \App\Service::where('status', 'Active')->get();
        ?>
        @foreach($services as $service)
        <div class="serve-grid col-md-4 col-sm-6 mt-lg-5 text-center">
          {!! $service->icon !!}
          <h4 class="my-lg-4 my-3">{{$service->title}}</h4>
          <p>{{$service->detail}}</p>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
<section class="banner-bottom-wthree bg-dark dotts py-lg-5 py-3">
  <div class="container">
    <div class="inner-sec-w3ls py-lg-5 py-md-5 py-3">
      <h3 class="tittle cen text-center mb-lg-5 mb-3">
        <span>Some Info</span> Our Stats
      </h3>
      <div class="stats row mt-5">
        <div class="col-md-3 stats_left counter_grid text-center">
          <p class="counter">145</p>
          <h4>Jobs Posted</h4>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="banner-bottom-wthree py-lg-5 py-md-5 py-3">
  <div class="container">
    <div class="inner-sec-w3ls py-lg-5 py-md-5 py-3">
      <h3 class="tittle text-center mb-lg-5 mb-3">
        <span>Some Info</span> Quick Career Tips
      </h3>
      <div class="row mt-5">
        <div class="card-deck">
          <div class="card">
            <img src="images/g1.jpg" alt="Card image cap" class="img-fluid card-img-top">
            <div class="card-body">
              <h5 class="card-title">Would Disruption Work for Your Business?</h5>
              <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit sedc dnmo eiusmod tempor incididunt ut labore et dolore .</p>
            </div>
            <div class="card-footer">
              <small class="text-muted">Last updated 3 mins ago</small>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
