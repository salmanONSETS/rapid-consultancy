@extends('layouts.app')
@section('content')
<ol class="breadcrumb justify-content-left">
    <li class="breadcrumb-item">
        <a href="index.html">Home</a>
	</li>
	<li class="breadcrumb-item active">Contact Us</li>
</ol>
<!-- banner-text -->
<!-- contact -->
<section class="banner-bottom-wthree pt-lg-5 pt-md-3 pt-3">
	<div class="inner-sec-w3ls pt-md-5 pt-md-3 pt-3">
		<h3 class="tittle text-center mb-lg-5 mb-3">
            <span>Get Intouch</span>Contact Us
        </h3>
		<div class="container">
			<div class="address row mb-5">
				<div class="col-lg-4 address-grid">
					<div class="row address-info">
					    <div class="col-md-3 address-left text-center">
							<i class="far fa-map"></i>
						</div>
						<div class="col-md-9 address-right text-left">
                            <h6 class="ad-info text-uppercase mb-2">Address</h6>
                            <?php
                            $addresses = \App\Address::where('status', 'Active')->get();
                            ?>
                            @foreach($addresses as $value)
                            <p>{{$value->address}}</p>
                            @endforeach
						</div>
					</div>
				</div>
				<div class="col-lg-4 address-grid">
					<div class="row address-info">
						<div class="col-md-3 address-left text-center">
							<i class="far fa-envelope"></i>
						</div>
						<div class="col-md-9 address-right text-left">
							<h6 class="ad-info text-uppercase mb-2">Email</h6>
							<p>Email :
                                <?php
                                $emails = \App\Email::where('status', 'Active')->get();
                                ?>
                                @foreach($emails as $email)
                                <a href="mailto:{{$email->email}}"> {{$email->email}}</a>
                                @endforeach
                            </p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 address-grid">
					<div class="row address-info">
						<div class="col-md-3 address-left text-center">
							<i class="fas fa-mobile-alt"></i>
						</div>
						<div class="col-md-9 address-right text-left">
                            <h6 class="ad-info text-uppercase mb-2">Phone</h6>
                            <?php
                            $contacts = \App\Contact::where('status', 'Active')->get();
                            ?>
                            @foreach($contacts as $contact)
                            <p>{{$contact->contact}}</p>
                            @endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
        <?php
        $map = \App\Map::where('status', 'Active')->first();
        ?>
        @if(!empty($map))
        <div class="col-md-6 map">
					<iframe src="{{$map->mapLink}}"
						allowfullscreen></iframe>
				</div>
        @endif
				<div class="col-md-6 main_grid_contact">
					<div class="form">
						<h4 class="mb-4 text-left">Send us a message</h4>
                        <form action="/messages" method="post">
                            {{ csrf_field() }}
							<div class="form-group">
								<label class="my-2">Name</label>
								<input class="form-control" type="text" name="name" placeholder="Name" required>
                            </div>
                            <div class="form-group">
								<label class="my-2">Phone <small>(e.g. 03001111111)</small></label>
								<input class="form-control" type="tel" name="phone" placeholder="Phone" required  pattern="[0-9]{11,20}">
							</div>
							<div class="form-group">
								<label>Email</label>
								<input class="form-control" type="email" name="email" placeholder="Email" required>
							</div>
							<div class="form-group">
								<label>Message</label>
								<textarea id="textarea" name="message" placeholder="Message" required></textarea>
							</div>
							<div class="input-group1">
								<input class="form-control" type="submit" value="Submit">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
