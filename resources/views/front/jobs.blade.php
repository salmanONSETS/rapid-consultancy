@extends('layouts.app')
@section('content')
<ol class="breadcrumb justify-content-left">
  <li class="breadcrumb-item">
    <a href="index.html">Home</a>
	</li>
	<li class="breadcrumb-item active">Jobs</li>
</ol>
<section class="banner-bottom-wthree pb-lg-5 pb-md-4 pb-3">
  <div class="container">
    <div class="banner-info-w3layouts text-center">
        <h3>
            <span style="color:#000000">Find the Right Job</span> .
            <span style="color:#000000">Right Now.</span>
        </h3>
        <p style="color:#e9c1a5">Your job search starts and ends with us.</p>
        <form action="/searchJobs" method="post" class="row">
          {{ csrf_field() }}
            <div class="col-md-3 banf">
                <input class="form-control" type="text" name="title" placeholder="Title">
            </div>
            <div class="col-md-3 banf">
                <select class="form-control" name="country">
                  <option value="">Select Country</option>
                  <?php
                  $countries = array();
                  $jobs = \App\Job::where('status', 'Active')->get();
                  foreach ($jobs as $key => $value) {
                    if(!in_array($value->country, $countries)){
                      array_push($countries, $value->country);
                    }
                  }
                  ?>
                  @foreach($countries as $country)
                  <option>{{$country}}</option>
                  @endforeach

                </select>
            </div>
            <div class="col-md-3 banf">
              <?php
              $categories = \App\Category::where('status', 'Active')->get();
              ?>
                <select class="form-control" name="categoryId">
                  <option value="">Select Category</option>
                  @foreach($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                  @endforeach
                </select>
            </div>
            <div class="col-md-3 banf">
                <button class="btn1 btn btn-block" type="submit">FIND JOB
                    <i class="fas fa-search"></i>
                </button>
            </div>
        </form>
    </div>
    <div class="inner-sec-w3ls py-lg-5  py-3">
      <h3 class="tittle text-center mb-lg-4 mb-3">
        <span>Some Info</span>Latest Job flow-positions
      </h3>
      <div class="tabs mt-5">
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <div class="menu-grids mt-4">
              <div class="row t-in">
                <div class="col-lg-12 text-info-sec">
                  <?php
                  if(!isset($searchedJobs)){
                    $featuredJobs = \App\Job::where('status', 'Active')->orderBy('created_at', 'desc')->get();
                  }
                  else{
                    $featuredJobs = $searchedJobs;
                  }
                  ?>
                  @foreach($featuredJobs as $featuredJob)
                  <div class="job-post-main row mb-3">
                    <div class="col-md-offset-2 col-md-8 job-post-info text-left">
                      <div class="job-post-icon">
                        <i class="fas fa-briefcase"></i>
                      </div>
                      <div class="job-single-sec">
                        <h4>
                          <a href="#">{{$featuredJob->title}}</a>
                        </h4>
                        <p class="my-2">{{$featuredJob->companyName}}</p>
                        <ul class="job-list-info d-flex">
                          <li>
                            <?php
                            $category = \App\Category::find($featuredJob->categoryId);
                            ?>
                            <i class="fas fa-briefcase"></i> {{$category->name}}
                          </li>
                          <li>
                            <i class="fas fa-map-marker-alt"></i> {{$featuredJob->country}}
                          </li>
                          <li>
                            <i class="fas fa-dollar-sign"></i> {{$featuredJob->salary}}
                          </li>
                        </ul>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="col-md-3 job-single-time text-right">
                      <a href="/apply/{{$featuredJob->id}}" class="aply-btn ">Appy Now</a>
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
