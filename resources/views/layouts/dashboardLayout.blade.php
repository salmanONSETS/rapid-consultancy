<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/logo.png') }}">
  <link rel="icon" type="image/png" href="{{ asset('img/logo.png') }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Rapid OEP Dashboard</title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- CSS Files -->
  <link href="{{ asset('../css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('../css/now-ui-dashboard.css?v=1.3.0') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{ asset('../demo/demo.css') }}" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="orange">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          CT
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          Creative Tim
        </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
        <?php
          $route = Route::currentRouteName();
        ?>
          <li @if(strpos($route, 'users') || strpos($route, 'users') === 0) class="active" @endif hidden>
            <a href="/users">
              <i class="now-ui-icons design_app"></i>
              <p>Users</p>
            </a>
          </li>
          <li @if(strpos($route, 'abouts') || strpos($route, 'abouts') === 0) class="active" @endif>
            <a href="/abouts">
              <i class="now-ui-icons design_app"></i>
              <p>About</p>
            </a>
          </li>
          <li @if(strpos($route, 'emails') || strpos($route, 'emails') === 0) class="active" @endif>
            <a href="/emails">
              <i class="now-ui-icons design_app"></i>
              <p>Emails</p>
            </a>
		      </li>
		      <li @if(strpos($route, 'contacts') || strpos($route, 'contacts') === 0) class="active" @endif>
            <a href="/contacts">
              <i class="now-ui-icons design_app"></i>
              <p>Contacts</p>
            </a>
          </li>
          <li @if(strpos($route, 'addresses') || strpos($route, 'addresses') === 0) class="active" @endif>
            <a href="/addresses">
              <i class="now-ui-icons design_app"></i>
              <p>Addresses</p>
            </a>
          </li>
          <li @if(strpos($route, 'socials') || strpos($route, 'socials') === 0) class="active" @endif>
            <a href="/socials">
              <i class="now-ui-icons design_app"></i>
              <p>Socials</p>
            </a>
          </li>
          <li @if(strpos($route, 'stats') || strpos($route, 'stats') === 0) class="active" @endif>
            <a href="/stats">
              <i class="now-ui-icons design_app"></i>
              <p>Stats</p>
            </a>
          </li>
          <li @if(strpos($route, 'messages') || strpos($route, 'messages') === 0) class="active" @endif>
            <a href="/messages">
              <i class="now-ui-icons design_app"></i>
              <p>Messages</p>
            </a>
          </li>
          <li @if(strpos($route, 'services') || strpos($route, 'services') === 0) class="active" @endif>
            <a href="/services">
              <i class="now-ui-icons design_app"></i>
              <p>Services</p>
            </a>
          </li>
          <li @if(strpos($route, 'selectionProcesses') || strpos($route, 'selectionProcesses') === 0) class="active" @endif>
            <a href="/selectionProcesses">
              <i class="now-ui-icons design_app"></i>
              <p>Selection Processes</p>
            </a>
          </li>
          <li @if(strpos($route, 'categories') || strpos($route, 'categories') === 0) class="active" @endif>
            <a href="/categories">
              <i class="now-ui-icons design_app"></i>
              <p>Categories</p>
            </a>
          </li>
          <li @if(strpos($route, 'teams') || strpos($route, 'teams') === 0) class="active" @endif>
            <a href="/teams">
              <i class="now-ui-icons design_app"></i>
              <p>Team</p>
            </a>
          </li>
          <li @if(strpos($route, 'careerTips') || strpos($route, 'careerTips') === 0) class="active" @endif>
            <a href="/careerTips">
              <i class="now-ui-icons design_app"></i>
              <p>Career Tips</p>
            </a>
          </li>
          <li @if(strpos($route, 'jobSideImages') || strpos($route, 'jobSideImages') === 0) class="active" @endif>
            <a href="/jobSideImages">
              <i class="now-ui-icons design_app"></i>
              <p>Job Side Images</p>
            </a>
          </li>
          <li @if(strpos($route, 'hows') || strpos($route, 'hows') === 0) class="active" @endif>
            <a href="/hows">
              <i class="now-ui-icons design_app"></i>
              <p>How It Works</p>
            </a>
          </li>
          <li @if(strpos($route, 'jobs') || strpos($route, 'jobs') === 0) class="active" @endif>
            <a href="/jobs">
              <i class="now-ui-icons design_app"></i>
              <p>Jobs</p>
            </a>
          </li>
          <li @if(strpos($route, 'cvs') || strpos($route, 'cvs') === 0) class="active" @endif>
            <a href="/cvs">
              <i class="now-ui-icons design_app"></i>
              <p>CV's</p>
            </a>
          </li>
          <li @if(strpos($route, 'images') || strpos($route, 'images') === 0) class="active" @endif>
            <a href="/images">
              <i class="now-ui-icons design_app"></i>
              <p>Images</p>
            </a>
          </li>
          <li @if(strpos($route, 'maps') || strpos($route, 'maps') === 0) class="active" @endif>
            <a href="/maps">
              <i class="now-ui-icons design_app"></i>
              <p>Maps</p>
            </a>
          </li>
          <li @if(strpos($route, 'notifications') || strpos($route, 'notifications') === 0) class="active" @endif>
            <a href="/notifications">
              <i class="now-ui-icons design_app"></i>
              <p>Notifications</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel" id="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link dropdown-toggle" href="#profile" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="now-ui-icons users_single-02"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Account</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="/updateProfile">Change Password</a>
                  <a class="dropdown-item" href="{{ url('/logout') }}"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Logout
                  </a>
                  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="panel-header panel-header-sm">
      </div>
      @yield('content')
      <footer class="footer">
        <div class="container-fluid">
          <div class="copyright" id="copyright">
            &copy;
            <script>
              document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>, Designed by
            <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{ asset('../js/core/jquery.min.js') }}"></script>
  <script src="{{ asset('../js/core/popper.min.js') }}"></script>
  <script src="{{ asset('../js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('../js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{ asset('../js/plugins/chartjs.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ asset('../js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('../js/now-ui-dashboard.min.js?v=1.3.0') }}" type="text/javascript"></script>
  <!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{ asset('../demo/demo.js') }}"></script>
</body>

</html>
