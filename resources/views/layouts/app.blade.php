<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Rapid Overseas Employement Promoters, Employement, Overseas, Job, Gulf">
    <meta name="author" content="ONSETS">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Rapid Overseas Employement Promoters</title>
    <link rel="icon" href="{{ asset('img/logo.png') }}" type="image/gif" sizes="16x16">
    <!-- Styles -->

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <link href="{{ asset('css/front/bootstrap.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ asset('css/front/style6.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ asset('css/front/style.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ asset('css/front/fontawesome-all.css') }}" rel="stylesheet" />
    <link href="//fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700" rel="stylesheet" />
    <link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
</head>
<body>
    <!-- banner-inner -->
    <div id="demo-1" class="page-content">
      <?php
      $header = \App\Image::where('status', 'Active')->where('page', 'Header')->first();
      ?>
      @if(!empty($header))
      <div class="dotts"
      style="background: url({{asset('imagesData/images/').'/'.$header->picturePath}}) no-repeat 0px 0px;
      background-size:cover;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      -moz-background-size: cover;
      min-height:300px;"
      >
      @else
      <div class="dotts"
      style="background: url({{asset('img/3.jpg')}}) no-repeat 0px 0px;
      background-size:cover;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      -moz-background-size: cover;
      min-height:300px;"
      >
      @endif
        <div class="header-top">
          <header>
            <div class="top-head ml-lg-auto text-center">
              <div class="row mr-0">
                <div class="col-md-6">
                  <span>Welcome Back!</span>
                </div>
                <div class="col-md-6 sign-btn">
                  <a href="#" data-toggle="modal" data-target="#exampleModalCenter">
                    <i class="fas fa-lock"></i> Sign In
                  </a>
                </div>
                <div class="col-md-3 sign-btn" hidden>
                  <a href="#" data-toggle="modal" data-target="#exampleModalCenter2">
                    <i class="far fa-user"></i> Register
                  </a>
                </div>
                <div class="search col-md-2" hidden>
                  <div class="mobile-nav-button">
                    <button id="trigger-overlay" type="button">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
                  <!-- open/close -->
                  <div class="overlay overlay-door">
                    <button type="button" class="overlay-close">
                      <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                    <form action="#" method="post" class="d-flex">
                      <input class="form-control" type="search" placeholder="Search here..." required="">
                      <button type="submit" class="btn btn-primary submit">
                        <i class="fas fa-search"></i>
                      </button>
                    </form>
                  </div>
                  <!-- open/close -->
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <nav class="navbar navbar-expand-lg navbar-light">
              <div class="logo">
                <h1>
                  <a class="navbar-brand" href="index.html">
                    <img src="{{ asset('./img/logo.png') }}" width="30" height="30" style="width:30px" /> Rapid OEP
                  </a>
                </h1>
              </div>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                  <i class="fas fa-bars"></i>
                </span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-lg-auto text-center">
                  <?php
                  $name = Route::currentRouteName();
                  ?>
                  <li @if($name) === '/' class="nav-item active" @else class="nav-item" @endif>
                    <a class="nav-link" href="/">Home
                      <span class="sr-only">(current)</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/viewJobs">Jobs</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/selectionProcess">Selection Process</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/ourProjects">Our Projects</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/viewTeam">Team</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/contactUs">Contact Us</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/aboutUs">About</a>
                  </li>
                </ul>
              </div>
            </nav>
          </header>
        </div>
        <!--/banner-info-w3layouts-->
        <div class="banner-info-w3layouts text-center">
        </div>
        <!--//banner-info-w3layouts-->
      </div>
    </div>
      @yield('content')
    <!--model-forms-->
    <!--/Login-->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login px-4 mx-auto mw-100">
              <h5 class="text-center mb-4">Login Now</h5>
              <form action="{{ url('/login') }}" method="post">
                {{ csrf_field() }}
                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                      <label class="mb-2">Email address</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" placeholder="Email" required  value="{{ old('email') }}">
                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                  </div>
                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                      <label class="mb-2">Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                  </div>
                  <div class="form-check mb-2">
                      <input type="checkbox" name="remember" class="form-check-input" id="exampleCheck1"{{ old('remember') ? 'checked' : ''}}>
                      <label class="form-check-label" for="exampleCheck1">Remember Me</label>
                  </div>
                  <button type="submit" class="btn btn-primary submit mb-4">Sign In</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--//Login-->
    <!--/Register-->
    <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login px-4 mx-auto mw-100">
              <h5 class="text-center mb-4">Register Now</h5>
              <form action="#" method="post">
                <div class="form-group">
                  <label>First name</label>
                  <input type="text" class="form-control" id="validationDefault01" placeholder="" required="">
                </div>
                <div class="form-group">
                  <label>Last name</label>
                  <input type="text" class="form-control" id="validationDefault02" placeholder="" required="">
                </div>
                <div class="form-group">
                  <label class="mb-2">Password</label>
                  <input type="password" class="form-control" id="password1" placeholder="" required="">
                </div>
                <div class="form-group">
                  <label>Confirm Password</label>
                  <input type="password" class="form-control" id="password2" placeholder="" required="">
                </div>
                <button type="submit" class="btn btn-primary submit mb-4">Register</button>
                <p class="text-center pb-4">
                  <a href="#">By clicking Register, I agree to your terms</a>
                </p>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--//Register-->

    <!--//model-form-->

    <!--footer -->
    <footer class="footer-emp-w3layouts bg-dark dotts py-lg-5 py-3">
      <div class="container-fluid px-lg-5 px-3">
        <div class="row footer-top">
          <div class="col-lg-4 footer-grid-wthree-w3ls">
            <div class="footer-title">
              <h3>About Us</h3>
            </div>
            <?php
            $about = \App\About::where('status', 'Active')->first();
            ?>
            <div class="footer-text">
              @if(!empty($about))
              <p>{{$about->detail}}</p>
              @endif
              <ul class="footer-social text-left mt-lg-4 mt-3">
                <?php
                $socials = \App\Social::where('status', 'Active')->get()
                ?>
                @foreach($socials as $value)
                <li class="mx-2">
                  <a href="{{$value->link}}">
                    {!! $value->icon !!}
                  </a>
                </li>
                @endforeach
              </ul>
            </div>
          </div>
          <div class="col-lg-4 footer-grid-wthree-w3ls">
            <div class="footer-title">
              <h3>Get in touch</h3>
            </div>
            <div class="contact-info">
              <h4>Location :</h4>
              <?php
              $address = \App\Address::where('status', 'Active')->where('type', 'Primary')->first();
              $contact = \App\Contact::where('status', 'Active')->where('type', 'Primary')->first();
              $email = \App\Email::where('status', 'Active')->Where('type', 'Primary')->first();
              ?>
              @if(!empty($address))
              <p>{{$address->address}}</p>
              @endif
              <div class="phone">
                <h4>Contact :</h4>
                <p>Phone : @if(!empty($contact)) {{$contact->contact}} @endif</p>
                <p>Email :
                  @if(!empty($email))
                  <a href="mailto:info@example.com">{{$email->email}}</a>
                  @endif
                </p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 footer-grid-wthree-w3ls">
            <div class="footer-title">
              <h3>Quick Links</h3>
            </div>
            <ul class="links">
              <li>
                <a href="/">Home</a>
              </li>
              <li>
                <a href="/viewJobs">Jobs</a>
              </li>
              <li>
                <a href="/selectionProcess">Selection Process</a>
              </li>
              <li>
                <a href="/ourProjects">Our Projects</a>
              </li>
              <li>
                <a href="/viewTeam">Team</a>
              </li>
            </ul>
            <ul class="links">
              <li>
                <a href="/contactUs">Contact Us</a>
              </li>
              <li>
                <a href="/aboutUs">About Us</a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="copyright mt-4">
          <p class="copy-right text-center" id="copyright">&copy;
            <script>
            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()));
            </script> Replenish. All Rights Reserved | Design by
            <a href="http://w3layouts.com/"> W3layouts </a> | Developed by
            <a href="http://www.onsets.co/" target="_blank"> ONSETS </a>
          </p>
        </div>
      </div>
    </footer>
    <!-- //footer -->
    <script src="{{ asset('js/front/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ asset('js/front/modernizr-2.6.2.min.js') }}"></script>
    <script src="{{ asset('js/front/jquery.zoomslider.min.js') }}"></script>
    <!--//slider-->
    <!--search jQuery-->
    <script src="{{ asset('js/front/classie-search.js') }}"></script>
    <script src="{{ asset('js/front/demo1-search.js') }}"></script>
    <!--//search jQuery-->
    <script>
            $(document).ready(function() {
                $(".dropdown").hover(
                    function() {
                        $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                        $(this).toggleClass('open');
                    },
                    function() {
                        $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                        $(this).toggleClass('open');
                    }
                );
            });
        </script>
        <!-- //dropdown nav -->
        <!-- password-script -->
        <script>
            window.onload = function() {
                document.getElementById("password1").onchange = validatePassword;
                document.getElementById("password2").onchange = validatePassword;
            }

            function validatePassword() {
                var pass2 = document.getElementById("password2").value;
                var pass1 = document.getElementById("password1").value;
                if (pass1 != pass2)
                    document.getElementById("password2").setCustomValidity("Passwords Don't Match");
                else
                    document.getElementById("password2").setCustomValidity('');
                //empty string means no validation error
            }
        </script>
        <!-- //password-script -->

        <!-- stats -->
        <script src="{{ asset('js/front/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('js/front/jquery.countup.js') }}"></script>
        <script>
            $('.counter').countUp();
        </script>
        <!-- //stats -->

        <!-- //js -->
        <script src="{{ asset('js/front/bootstrap.js') }}"></script>
        <!--/ start-smoth-scrolling -->
        <script src="{{ asset('js/front/move-top.js') }}"></script>
        <script src="{{ asset('js/front/easing.js') }}"></script>
        <script>
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event) {
                    event.preventDefault();
                    $('html,body').animate({
                        scrollTop: $(this.hash).offset().top
                    }, 900);
                });
            });
        </script>
        <script>
            $(document).ready(function() {
                /*
                						var defaults = {
                							  containerID: 'toTop', // fading element id
                							containerHoverID: 'toTopHover', // fading element hover id
                							scrollSpeed: 1200,
                							easingType: 'linear'
                						 };
                						*/

                $().UItoTop({
                    easingType: 'easeOutQuart'
                });

            });
        </script>
        <!--// end-smoth-scrolling -->
        <script src="{{ asset('./js/front/bootstrap-notify.js') }}" type="text/javascript"></script>
        @if(session('message'))
        <script type="text/javascript">
        $(document).ready(function(){
          $.notify({
            message: "{{session('message')}}"
          },
          {
            type: 'success',
            allow_dismiss: true,
            timer: 4000
          });
        });
        </script>
        @endif
</body>
</html>
