@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($notification))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Notification</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/notifications" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Useage</label>
                    <select class="form-control" name="useage" required>
                      <option>Select Useage</option>
                      <option value="Contact">For Contact Form</option>
                      <option value="Apply">For Apply Form</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Message</label>
                    <input type="text" class="form-control" placeholder="Message" name="message">
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Notification</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/notifications/<?php echo $notification->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Useage</label>
                    <select class="form-control" name="useage" required>
                      <option>Select Useage</option>
                      <option value="Contact" @if($notification->useage == 'Contact') selected @endif>For Contact Form</option>
                      <option value="Apply" @if($notification->useage == 'Apply') selected @endif>For Apply Form</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Message</label>
                    <input type="text" class="form-control" placeholder="Message" name="message" required value="{{$notification->message}}">
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Notifications</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Useage</th>
                    <th>Message</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $notifications = \App\Notification::all();
                  ?>
                  @foreach($notifications as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td>{{$value->useage}}</td>
                    <td>{{$value->message}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/notifications_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/notifications/<?php echo $value->id;?>/edit">Edit</a>
                      <form action="{{ route('notifications.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
