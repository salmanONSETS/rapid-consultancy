@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($social))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Social</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/socials" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Link</label>
                    <input type="url" class="form-control" placeholder="Link" name="link" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Icon</label>
                    <input type="text" class="form-control" placeholder="Icon" name="icon" required>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Social</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/socials/<?php echo $social->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Link</label>
                    <input type="url" class="form-control" placeholder="Link" name="link" required value="{{$social->link}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Icon (Current Icon: {!! $social->icon !!})</label>
                    <input type="text" class="form-control" placeholder="Icon" name="icon" value="{{$social->icon}}" required>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Socials</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Link</th>
                    <th>Icon</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $socials = \App\Social::all();
                  ?>
                  @foreach($socials as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td><a href="{{$value->link}}">{{$value->link}}</a></td>
                    <td>{{$value->icon}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/socials_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/socials/<?php echo $value->id;?>/edit">Edit</a>
                      <form action="{{ route('socials.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
