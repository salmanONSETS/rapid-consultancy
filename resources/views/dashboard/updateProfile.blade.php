@extends('layouts.dashboardLayout')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Update Profile</h4>
            </div>
            <div class="card-body">
              <form class="form-horizontal" role="form" method="POST" action="{{ url('/saveProfile') }}">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="email">E-Mail Address</label>
                        <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required autofocus>
                        <input type="text" name="id" value="{{$user->id}}" hidden />
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="password" class="col-md-4 control-label">New Password</label>
                        <input id="password" type="password" class="form-control" name="password" placeholder="New Password" minlength="6">
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-success btn-round pull-right">Submit</button>
              </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
