@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($address))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Address</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/addresses" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Address</label>
                    <input type="text" class="form-control" placeholder="Address" name="address" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Type</label>
                    <select class="form-control" name="type" required>
                      <option>Primary</option>
                      <option>Secondary</option>
                    </select>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Address</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/addresses/<?php echo $address->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Address</label>
                    <input type="text" class="form-control" placeholder="Address" name="address" value="{{$address->address}}" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" value="{{$address->title}}">
                  </div>
                </div>
                <div class="col-md-4 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Type</label>
                    <select class="form-control" name="type" required value="{{$address->type}}">
                      <option @if($address->type == 'Primary') selected @endif>Primary</option>
                      <option @if($address->type == 'Secondary') selected @endif>Secondary</option>
                    </select>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Addresses</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Address</th>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $addresses = \App\Address::all();
                  ?>
                  @foreach($addresses as $address)
                  <tr>
                    <td>{{$address->id}}</td>
                    <td>{{$address->address}}</td>
                    @if(!empty($address->title))
                    <td>{{$address->title}}</td>
                    @else
                    <td></td>
                    @endif
                    <td>{{$address->type}}</td>
                    <td>{{$address->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/addresses_changestatus/<?php echo $address->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/addresses/<?php echo $address->id;?>/edit">Edit</a>
                      <form action="{{ route('addresses.destroy', $address->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
