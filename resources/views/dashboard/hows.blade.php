@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($how))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add How It Work Step</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/hows" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Serial Number</label>
                    <input type="number" min="0" class="form-control" placeholder="Serial Number" required name="serialNumber">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png" required>
                    </button>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Detail</label>
                    <textarea class="form-control" placeholder="Detail" name="detail"></textarea>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit How It Work Step</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-3">
                <img src="{{asset('howsData/images/').'/'.$how->picturePath}}" alt="How It Works Image" class="img img-responsive" />
              </div>
            </div>
            <form role="form" action="/hows/<?php echo $how->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required value="{{$how->title}}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Serial Number</label>
                    <input type="number" min="0" class="form-control" placeholder="Serial Number" required name="serialNumber" value="{{$how->serialNumber}}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png">
                    </button>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Detail</label>
                    <textarea class="form-control" placeholder="Detail" name="detail">{{$how->detail}}</textarea>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">How It Works Steps</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Detail</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $hows = \App\How::all();
                  ?>
                  @foreach($hows as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td><img src="{{asset('howsData/images/').'/'.$value->picturePath}}" alt="How It Work Step Image" width="100" height="100" /></td>
                    <td>{{$value->title}}</td>
                    <td>{{$value->detail}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/hows_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/hows/<?php echo $value->id;?>/edit">Edit</a>
                      <form action="{{ route('hows.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
