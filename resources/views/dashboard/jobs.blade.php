@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($job))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Job</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/jobs" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Company Name</label>
                    <input type="text" class="form-control" placeholder="Company Name" name="companyName" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Country</label>
                    <input type="text" class="form-control" placeholder="Country" name="country" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Salary</label>
                    <input type="text" class="form-control" placeholder="Salary" name="salary" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Category</label>
                    <select class="form-control" name="categoryId" required>
                      <?php
                      $categories = \App\Category::where('status', 'Active')->get();
                      ?>
                      @foreach($categories as $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Featured</label>
                    <select class="form-control" name="featured" required>
                      <option value="true">Yes</option>
                      <option value="false">No</option>
                    </select>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Job</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/jobs/<?php echo $job->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required value="{{$job->title}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Company Name</label>
                    <input type="text" class="form-control" placeholder="Company Name" name="companyName" required value="{{$job->companyName}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Country</label>
                    <input type="text" class="form-control" placeholder="Country" name="country" required value="{{$job->country}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Salary</label>
                    <input type="text" class="form-control" placeholder="Salary" name="salary" required value="{{$job->salary}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Category</label>
                    <select class="form-control" name="categoryId" required>
                      <?php
                      $categories = \App\Category::where('status', 'Active')->get();
                      ?>
                      @foreach($categories as $category)
                      <option @if($job->categoryId == $category->id) selected @endif value="{{$category->id}}">{{$category->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Featured</label>
                    <select class="form-control" name="featured" required>
                      <option @if($job->featured == 1) selected @endif value="true">Yes</option>
                      <option @if($job->featured == 0) selected @endif value="false">No</option>
                    </select>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Jobs</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Company</th>
                    <th>Country</th>
                    <th>Salary</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $jobs = \App\Job::all();
                  ?>
                  @foreach($jobs as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td>{{$value->title}}</td>
                    <td>{{$value->companyName}}</td>
                    <td>{{$value->country}}</td>
                    <td>{{$value->salary}}</td>
                    <?php
                    $category = \App\Category::find($value->categoryId);
                    ?>
                    <td>{{$category->name}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/jobs_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/jobs/<?php echo $value->id;?>/edit">Edit</a>
                      <form action="{{ route('jobs.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
