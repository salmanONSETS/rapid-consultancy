@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($selectionProcess))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Selection Process</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/selectionProcesses" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Serial Number</label>
                    <input type="number" min="0" class="form-control" placeholder="Serial Number" required name="serialNumber">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Detail</label>
                    <input type="text" class="form-control" placeholder="Detail" name="detail">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png" required>
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Selection Process</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-3">
                <img src="{{asset('selectionProcessesData/images/').'/'.$selectionProcess->picturePath}}" alt="Select Process Image" class="img img-responsive" />
              </div>
            </div>
            <form role="form" action="/selectionProcesses/<?php echo $selectionProcess->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required value="{{$selectionProcess->title}}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Serial Number</label>
                    <input type="number" min="0" class="form-control" placeholder="Serial Number" required name="serialNumber" value="{{$selectionProcess->serialNumber}}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Detail</label>
                    <input type="text" class="form-control" placeholder="Detail" name="detail" value="{{$selectionProcess->detail}}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <small>If you want to change the old image you can upload the new one it will replace the old one. Otherwise, leave it empty.</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png">
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Selection Processes</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Detail</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $selectionProcesses = \App\SelectionProcess::all();
                  ?>
                  @foreach($selectionProcesses as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td><img src="{{asset('selectionProcessesData/images/').'/'.$value->picturePath}}" alt="Selection Process Image" width="100" height="100" /></td>
                    <td>{{$value->title}}</td>
                    <td>{{$value->detail}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/selectionProcesses_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/selectionProcesses/<?php echo $value->id;?>/edit">Edit</a>
                      <form action="{{ route('selectionProcesses.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
