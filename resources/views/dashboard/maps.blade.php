@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <?php
        $maps = \App\Map::all();
        ?>
        @if(!isset($map) && $maps->isEmpty())
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Map</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/maps" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Link</label>
                    <input type="url" class="form-control" placeholder="Link" name="link" required>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Map</h5>
          </div>
          <div class="card-body">
            <?php
            $map = \App\Map::first();
            ?>
            <form role="form" action="/maps/<?php echo $map->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Link</label>
                    <input type="url" class="form-control" placeholder="Link" name="link" required value="{{$map->mapLink}}">
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        @if($maps->isEmpty())
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Maps</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Link</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $maps = \App\Map::all();
                  ?>
                  @foreach($maps as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td><a href="{{$value->mapLink}}">Link</a></td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/maps_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/maps/<?php echo $value->id;?>/edit">Edit</a>
                      <form action="{{ route('maps.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>

@endsection
