@extends('layouts.dashboardlayout')
@section('content')
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($contact))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Contact</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/contacts" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Contact</label>
                    <input type="tel" class="form-control" placeholder="Contact" name="contact" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Type</label>
                    <select class="form-control" name="type" required>
                      <option>Primary</option>
                      <option>Secondary</option>
                    </select>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Contact</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/contacts/<?php echo $contact->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Contact</label>
                    <input type="tel" class="form-control" placeholder="Contact" name="contact" required value="{{$contact->contact}}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" value="{{$contact->title}}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Type</label>
                    <select class="form-control" name="type" required value="{{$contact->type}}">
                      <option @if($contact->type == 'Primary') selected @endif>Primary</option>
                      <option @if($contact->type == 'Secondary') selected @endif>Secondary</option>
                    </select>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Contacts</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Contact</th>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $contacts = \App\Contact::all();
                  ?>
                  @foreach($contacts as $contact)
                  <tr>
                    <td>{{$contact->id}}</td>
                    <td>{{$contact->contact}}</td>
                    @if(!empty($contact->title))
                    <td>{{$contact->title}}</td>
                    @else
                    <td></td>
                    @endif
                    <td>{{$contact->type}}</td>
                    <td>{{$contact->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/contacts_changestatus/<?php echo $contact->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/contacts/<?php echo $contact->id;?>/edit">Edit</a>
                      <form action="{{ route('contacts.destroy', $contact->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
