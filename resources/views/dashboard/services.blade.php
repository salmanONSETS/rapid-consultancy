@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($service))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Service</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/services" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Icon</label>
                    <input type="text" class="form-control" placeholder="Icon" name="icon" required>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Detail</label>
                    <textarea name="detail" placeholder="Detail" class="form-control"></textarea>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Service</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/services/<?php echo $service->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" required value="{{$service->title}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Icon (Current Icon: {!! $service->icon !!})</label>
                    <input type="text" class="form-control" placeholder="Icon" name="icon" value="{{$service->icon}}" required>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Detail</label>
                    <textarea name="detail" placeholder="Detail" class="form-control">{{$service->detail}}</textarea>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Services</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Icon</th>
                    <th>Detail</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $services = \App\Service::all();
                  ?>
                  @foreach($services as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td>{{$value->title}}</td>
                    <td>{!! $value->icon !!}</td>
                    <td>{{$value->detail}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/services_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/services/<?php echo $value->id;?>/edit">Edit</a>
                      <form action="{{ route('services.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
