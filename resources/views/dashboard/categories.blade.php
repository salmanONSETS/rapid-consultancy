@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($category))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Category</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/categories" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" placeholder="Name" name="name" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Icon</label>
                    <input type="text" class="form-control" placeholder="Icon" name="icon" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png" required>
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Category</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-3">
                <img src="{{asset('categoriesData/images/').'/'.$category->picturePath}}" alt="Category Image" class="img img-responsive" />
              </div>
            </div>
            <form role="form" action="/categories/<?php echo $category->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" placeholder="Name" name="name" required value="{{$category->name}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Icon</label>
                    <input type="text" class="form-control" placeholder="Icon" required name="icon" value="{{$category->icon}}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <small>If you want to change the old image you can upload the new one it will replace the old one. Otherwise, leave it empty.</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png">
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Selection Processes</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $categories = \App\Category::all();
                  ?>
                  @foreach($categories as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td><img src="{{asset('categoriesData/images/').'/'.$value->picturePath}}" alt="Category Image" width="100" height="100" /></td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/categories_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/categories/<?php echo $value->id;?>/edit">Edit</a>
                      <form action="{{ route('categories.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
