@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($teamMember))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Team Member</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/teams" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" placeholder="Name" name="name" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Designation</label>
                    <input type="text" class="form-control" placeholder="Designation" name="designation" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png" required>
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Team Member</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-3">
                <img src="{{asset('teamsData/images/').'/'.$teamMember->picturePath}}" alt="Team Member Image" class="img img-responsive" />
              </div>
            </div>
            <form role="form" action="/teams/<?php echo $teamMember->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" placeholder="Name" name="name" required value="{{$teamMember->name}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Designation</label>
                    <input type="text" class="form-control" placeholder="Designation" required name="designation" value="{{$teamMember->designation}}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <small>If you want to change the old image you can upload the new one it will replace the old one. Otherwise, leave it empty.</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png">
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Team Members</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $teamMembers = \App\Team::all();
                  ?>
                  @foreach($teamMembers as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td><img src="{{asset('teamsData/images/').'/'.$value->picturePath}}" alt="Team Member Image" width="100" height="100" /></td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->designation}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/teams_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/teams/<?php echo $value->id;?>/edit">Edit</a>
                      <form action="{{ route('teams.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
