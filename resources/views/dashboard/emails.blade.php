@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($email))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Email</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/emails" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" placeholder="Email" name="email" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Type</label>
                    <select class="form-control" name="type" required>
                      <option>Primary</option>
                      <option>Secondary</option>
                    </select>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Email</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/emails/<?php echo $email->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" placeholder="Email" name="email" required value="{{$email->email}}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" placeholder="Title" name="title" value="{{$email->title}}">
                  </div>
                </div>
                <div class="col-md-4 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Type</label>
                    <select class="form-control" name="type" required>
                      @if($email->type == 'Primary')
                      <option selected>Primary</option>
                      <option>Secondary</option>
                      @else
                      <option>Primary</option>
                      <option selected>Secondary</option>
                      @endif
                    </select>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Emails</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Email</th>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $emails = \App\Email::all();
                  ?>
                  @foreach($emails as $email)
                  <tr>
                    <td>{{$email->id}}</td>
                    <td>{{$email->email}}</td>
                    @if(!empty($email->title))
                    <td>{{$email->title}}</td>
                    @else
                    <td></td>
                    @endif
                    <td>{{$email->type}}</td>
                    <td>{{$email->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/emails_changestatus/<?php echo $email->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/emails/<?php echo $email->id;?>/edit">Edit</a>
                      <form action="{{ route('emails.destroy', $email->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
