@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(!isset($image))
        <div class="card">
          <div class="card-header">
            <h5 class="title">Add Image</h5>
          </div>
          <div class="card-body">
            <form role="form" action="/images" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Direction</label>
                    <select class="form-control" name="page">
                      <option>Home</option>
                      <option>Header</option>
                      <option hidden>About</option>
                      <option hidden>Team</option>
                      <option hidden>Contact</option>
                      <option hidden>Selection Process</option>
                      <option hidden>Jobs</option>
                      <option hidden>Our Projects</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Select Number For Welcome Screen Images</label>
                    <select class="form-control" name="number">
                      <option value="">Select Number</option>
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png" required>
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @else
        <div class="card">
          <div class="card-header">
            <h5 class="title">Edit Image</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-3">
                <img src="{{asset('imagesData/images/').'/'.$image->picturePath}}" alt="Header Image" class="img img-responsive" />
              </div>
            </div>
            <form role="form" action="/images/<?php echo $image->id;?>" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{method_field('PUT')}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Direction</label>
                    <select class="form-control" name="page">
                      <option @if($image->page === 'Home') selected @endif>Home</option>
                      <option @if($image->page === 'Header') selected @endif>About</option>
                      <option @if($image->page === 'About') selected @endif hidden>About</option>
                      <option @if($image->page === 'Contact') selected @endif hidden>Contact</option>
                      <option @if($image->page === 'Team') selected @endif hidden>Team</option>
                      <option @if($image->page === 'Selection Process') selected @endif hidden>Selection Process</option>
                      <option @if($image->page === 'Our Projects') selected @endif hidden>Our Projects</option>
                      <option @if($image->page === 'Jobs') selected @endif hidden>Jobs</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Select Number For Welcome Screen Images</label>
                    <select class="form-control" name="number">
                      <option value="">Select Number</option>
                      <option @if($image->number === '1') selected @endif>1</option>
                      <option @if($image->number === '2') selected @endif>2</option>
                      <option @if($image->number === '3') selected @endif>3</option>
                      <option @if($image->number === '4') selected @endif>4</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Image</label><small> (Only .png, .jpg, .jpeg accepted)</small><br>
                    <button class="btn btn-primary">
                      Choose File<input type="file" class="form-control" name="image" accept=".jpg,.jpeg,.png">
                    </button>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-round btn-success pull-right">Submit</button>
            </form>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Images</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Page</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $images = \App\Image::all();
                  ?>
                  @foreach($images as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td><img src="{{asset('imagesData/images/').'/'.$value->picturePath}}" alt="Header Image" width="100" height="100" /></td>
                    <td>{{$value->page}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/images_changestatus/<?php echo $value->id;?>" hidden>Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/images/<?php echo $value->id;?>/edit" hidden>Edit</a>
                      <form action="{{ route('images.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
