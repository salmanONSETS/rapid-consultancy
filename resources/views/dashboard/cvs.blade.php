@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(isset($cv))
        <div class="card">
          <div class="card-header">
            <h5 class="title">View Candidate</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Name</label>
                  <p>{{$cv->name}}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Phone</label>
                  <p>{{$cv->phone}}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Email</label>
                  <p>{{$cv->email}}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>CV</label>
                  <p><a href="{{asset('cvsData/documents/').'/'.$cv->cvPath}}" target="_blank">CV</a></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Job Status</label>
                  <p>{{$cv->jobStatus}}</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Comments</label>
                  <p>{{$cv->comments}}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">CV's</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $cvs = \App\CV::all();
                  ?>
                  @foreach($cvs as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->phone}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-default btn-sm" href="/cvs_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <a type="link" class="btn btn-info btn-sm" href="/cvs/<?php echo $value->id;?>">View</a>
                      <form action="{{ route('cvs.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete" />
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
