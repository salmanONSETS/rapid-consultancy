@extends('layouts.dashboardlayout')
@section('content')

  <div class="content">
    <div class="row">
      <div class="col-md-12">
        @if(isset($message))
        <div class="card">
          <div class="card-header">
            <h5 class="title">View Message</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-4">
                <label>Name:</label>
                @if(!empty($message->name))
                <span>{{$message->name}}</span>
                @endif
              </div>
              <div class="col-md-4">
                <label>Phone:</label>
                @if(!empty($message->phone))
                <span>{{$message->phone}}</span>
                @endif
              </div>
              <div class="col-md-4">
                <label>Email:</label>
                @if(!empty($message->email))
                <span>{{$message->email}}</span>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label>Message:</label>
                @if(!empty($message->message))
                <span>{{$message->message}}</span>
                @endif
              </div>
            </div>
          </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Messages</h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Status</th>
                    <th class="text-right">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $messages = \App\Message::all();
                  ?>
                  @foreach($messages as $value)
                  <tr>
                    <td>{{$value->id}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{$value->phone}}</td>
                    <td>{{$value->status}}</td>
                    <td class="text-right">
                      <a type="link" class="btn btn-primary btn-sm" href="/messages/<?php echo $value->id; ?>">View</a>
                      <a type="link" class="btn btn-default btn-sm" href="/messages_changestatus/<?php echo $value->id;?>">Change Status</a>
                      <form action="{{ route('messages.destroy', $value->id) }}" method="post" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
