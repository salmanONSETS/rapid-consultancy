@extends('layouts.dashboardLayout')
@section('content')
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Users</h4>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <th>
                  Name
                </th>
                <th>
                  Email
                </th>
                <th class="text-right">
                  Control Section
                </th>
              </thead>
              <tbody>
                <?php
                $users = \App\User::all();
                ?>
                @foreach($users as $value)
                <tr>
                  <td>
                    {{$value->name}}
                  </td>
                  <td>
                    {{$value->email}}
                  </td>
                  <td class="text-right">
                    $36,738
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
