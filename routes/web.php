<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/aboutUs', function () {
    return view('front.about');
});

Route::get('/contactUs', function () {
    return view('front.contact');
});

Route::get('/viewTeam', function () {
    return view('front.team');
});

Route::get('/viewJobs', function () {
    return view('front.jobs');
});

Route::get('/apply/{id}', function ($id) {
    return view('front.apply', ['jobId' => $id]);
});

Route::get('/categoryJobs/{id}', 'JobController@jobsOfCategory');

Route::get('/selectionProcess', function () {
    return view('front.selectionProcess');
});

Route::post('/searchJobs', 'JobController@searchJobs');

Route::post('/applyForJob', 'CVController@applyForJob');

Route::get('/home', function(){
  return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index');
Route::post('/saveProfile', 'UserController@saveProfile');
Route::get('/updateProfile', 'UserController@updateProfile');

//Users
Route::resource('users', 'UserController');

//About us
Route::resource('/abouts', 'AboutController');
Route::get('/abouts_changestatus/{id}', 'AboutController@changestatus');
Route::get('/deleteAboutImage/{id}', 'AboutController@deleteAboutImage');

//Email
Route::resource('/emails', 'EmailController');
Route::get('/emails_changestatus/{id}', 'EmailController@changestatus');

//Contact
Route::resource('/contacts', 'ContactController');
Route::get('/contacts_changestatus/{id}', 'ContactController@changestatus');

//Address
Route::resource('/addresses', 'AddressController');
Route::get('/addresses_changestatus/{id}', 'AddressController@changestatus');

//Social
Route::resource('/socials', 'SocialController');
Route::get('/socials_changestatus/{id}', 'SocialController@changestatus');

//Stat
Route::resource('/stats', 'StatController');
Route::get('/stats_changestatus/{id}', 'StatController@changestatus');

//Message
Route::resource('/messages', 'MessageController');
Route::get('/messages_changestatus/{id}', 'MessageController@changestatus');

//Services
Route::resource('/services', 'ServiceController');
Route::get('/services_changestatus/{id}', 'ServiceController@changestatus');

//Selection Process
Route::resource('/selectionProcesses', 'SelectionProcessController');
Route::get('/selectionProcesses_changestatus/{id}', 'SelectionProcessController@changestatus');

//Categories
Route::resource('/categories', 'CategoryController');
Route::get('/categories_changestatus/{id}', 'CategoryController@changestatus');

//Teams
Route::resource('/teams', 'TeamController');
Route::get('/teams_changestatus/{id}', 'TeamController@changestatus');

//CareerTips
Route::resource('/careerTips', 'CareerTipController');
Route::get('/careerTips_changestatus/{id}', 'CareerTipController@changestatus');

//Job Side Images
Route::resource('/jobSideImages', 'JobSideImageController');
Route::get('/jobSideImages_changestatus/{id}', 'JobSideImageController@changestatus');

//How It Works
Route::resource('/hows', 'HowController');
Route::get('/hows_changestatus/{id}', 'HowController@changestatus');

//Jobs
Route::resource('/jobs', 'JobController');
Route::get('/jobs_changestatus/{id}', 'JobController@changestatus');

//CVs
Route::resource('/cvs', 'CVController');
Route::get('/cvs_changestatus/{id}', 'CVController@changestatus');

//Images
Route::resource('/images', 'ImageController');
Route::get('/images_changestatus/{id}', 'ImageController@changestatus');

//Maps
Route::resource('/maps', 'MapController');
Route::get('/maps_changestatus/{id}', 'MapController@changestatus');

//Notifications
Route::resource('/notifications', 'NotificationController');
Route::get('/notifications_changestatus/{id}', 'NotificationController@changestatus');
